%% BF weights for TABs ARTS
%
% This script implements the equation on the third line on slide 16 of the
% presentation "Beamforming for ARTS" given by Stefan Wijnholds during an
% ARTS project meeting on April 4, 2018. The script produces a matrix
% containing the phases of the beamformer weights of size Ndish x NTAB,
% where Ndish is the number of dishes and NTAB is the number of TABs. It is
% assumed that the dishes for a Uniform Linear Array and that the TABs are
% equidistantly spaced between center of the compound beam and the first
% grating response. It is also assumed that the delay is already
% compensated for the center of the compound beam.
%
% SJW, 4 April 2018

%% start with a clean workspace
clear
close all

%% calculate phases
Ndish = 10;
NTAB = 12;
phase = zeros(Ndish, NTAB);

% determine beta, taking into account the TAB indexing conventions for ARTS
TABidx = -floor(NTAB/2):floor((NTAB-1)/2);
beta = TABidx / NTAB;

% define dish locations measured in integer multiples of the common
% quotient baseline
n = 0:(Ndish-1);

% calculate phases
phase = 2 * pi * n.' * beta;
phase_n = n.' * beta;

% show result
imagesc(phase_n)
colorbar
title(['BF weight phases * 2*pi [rad]'])
xlabel(['TABs']);
ylabel(['Dishes']);
