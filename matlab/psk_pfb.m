%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: Eric Kooistra, 2016 (based on one_pfb.m)
%
% Purpose : Simulate polyphase filterbank response on PSK input
%
% Description :
%
% * See one_pfb.m for general description of the polyphase filterbank
%   simulation.
%
% * Simple PSK modelling scheme to model DAB
%   The sinusiod can be phase shift key (PSK) modulated by setting tb.psk
%   > 0. When tb.psk = 0 then no PSK of the sinusoid, else tb.psk sets
%   the number of PSK phases (2, 4, 8, ...). The simple PSK modelling 
%   scheme is used as very simple model for DAB (digital audio broadcast).
%   The PSK rate is set relative to fsub by tb.psk_rate. The phase shift
%   can be applied in a step, but more realisticly it is applied smoothly
%   using tb.psk_smooth = 1 to keep the PSK signal narrow band. The 
%   simulations show that the PSK spectrum gets a skirt but is not
%   replicated relative to fs.
%
%
clear all;
close all;
fig=0;

%% Selectable test bench settings
tb.model_filterbank       = 'Free choice';         % 'LOFAR' = use fixed settings of LOFAR subband filterbank, else use free choice of tb.nof_subbands and FIR coefficients
%tb.model_filterbank       = 'LOFAR';
if strcmp(tb.model_filterbank, 'LOFAR')
    tb.model_quantization = 'fixed point';
    tb.nof_subbands       = 512;
else
    tb.model_quantization = 'floating point';
    %tb.model_quantization = 'fixed point';
    tb.nof_subbands = 64;
end

% Input signal 'sinusoid' carrier frequency
tb.subband_wg        = 7;           % subband range 0:tb.nof_subbands-1, can be fraction to have any sinusoid frequency
%tb.subband_wg        = 1.55;
%tb.subband_wg        = 12;

% Use PSK modulation of the 'sinusoid' as a simple model for DAB
tb.psk               = 0;
tb.psk               = 4;              % 0 = no PSK of the sinusoid, else number of PSK phases (2, 4, 8, ...) used as very simple model for DAB
tb.psk_rate          = 9;              % PSK frequency normalized to fsub
%tb.psk_rate          = 1/3;            % PSK frequency normalized to fsub
tb.psk_smooth        = 0;              % 0 = apply phase shift in one step, else apply phase shift smoothly to keep the PSK signal narrow band

% Model a frequency sweep of the 'sinusoid'
tb.chirp             = 0;              % 0 = use fixed tb.subband_wg frequency, else increment WG frequency every block to have chirp frequency sweep
%tb.chirp             = 1;
if tb.chirp
    tb.nof_tsub      = 500;            % number of subband periods to simulate
else
    tb.nof_tsub      = 100;            % number of subband periods to simulate
end

tb.plot_per_block    = 0;              % 1 = plot spectrum for each block in time, else skip this plot to save time
%tb.plot_per_block    = 1;


%% Derived test bench settings
tb.nof_complex       = 2;
tb.subband_i         = floor(tb.subband_wg);            % natural subband index in range 0:tb.nof_subbands-1
tb.subband_I         = tb.subband_i+1;                  % equivalent Matlab indexing for range 1:tb.nof_subbands
if tb.subband_i<tb.nof_subbands-1                       % determine index of next neighbour subband
    tb.subband_iplus1 = tb.subband_i+1;
else
    tb.subband_iplus1 = 0;                              % wrap subband index tb.nof_subbands to subband index 0
end
tb.subband_Iplus1    = tb.subband_iplus1+1;             % equivalent Matlab index for next neighbour subband in range 1:tb.nof_subbands
tb.subband_fft_size  = tb.nof_complex*tb.nof_subbands;  % subband filterbank real FFT

fs                   = 1;                       % normalized sample frequency
fsub                 = fs/tb.subband_fft_size;  % subband frequency relative to fs

disp(sprintf (['Test bench settings:\n', ...
               '. Model filterbank                       = %s\n', ...
               '. Model coefficients and data            = %s\n', ...
               '. Number of subbands                     = %d\n', ...
               '. Number of subband periods to simulate  = %d\n', ...
               '. Subband filterbank real FFT size       = %d\n', ...
               '. WG subband frequency                   = %f\n'], ...
               tb.model_filterbank, tb.model_quantization, tb.nof_subbands, tb.nof_tsub, tb.subband_fft_size, tb.subband_wg))


%% Waveform generator
ctrl_wg.signal = 'sinusoid';
ctrl_wg.complex = false;
ctrl_wg.block_nr   = 0;
ctrl_wg.block_size = tb.subband_fft_size;
ctrl_wg.sop = 0;
ctrl_wg.psk = tb.psk;
ctrl_wg.psk_smooth = tb.psk_smooth;
ctrl_wg.dpsk_phase = 0;
ctrl_wg.period = floor(ctrl_wg.block_size/tb.psk_rate);
if strcmp(tb.model_quantization, 'floating point')
    ctrl_wg.data_w = 0;
    lsb = 1/2^8;                            % Assume 8 bit ADC to have reference for AGWN level
else
    ctrl_wg.data_w = 8;
    lsb = 1/2^ctrl_wg.data_w;
end
ctrl_wg.agwn_sigma        = 2.6*lsb;
ctrl_wg.agwn_sigma        = 0;              % AGWN sigma
ctrl_wg.ampl = 1;                           % full scale is 1, ampl > 1 causes analogue clipping
if ctrl_wg.agwn_sigma>0
    ctrl_wg.ampl = 1-5*ctrl_wg.agwn_sigma;
    ctrl_wg.ampl = 0.9;
end
if tb.chirp
    ctrl_wg.df = 0.2*fsub;                 % increment freq by df per block to create chirp
else
    ctrl_wg.df = 0;
end
if ctrl_wg.df == 0
    ctrl_wg.freq = tb.subband_wg*fsub;      % start WG at tb.subband_wg
else
    ctrl_wg.freq = (tb.subband_i-1)*fsub;   % start WG about one subband before tb.subband_wg to account for impulse response of several tsub
end
ctrl_wg.phase = 0;                          % Phase offset normalized to 2pi
if ctrl_wg.freq == 0 && ctrl_wg.df == 0
    ctrl_wg.offset = 1;                     % DC offset 
else
    ctrl_wg.offset = 0;                     % DC offset
end

%% Delay tracking
ctrl_dt.block_size = tb.subband_fft_size;
ctrl_dt.buffer     = zeros(1, 2*ctrl_dt.block_size);
ctrl_dt.dt         = 0;       % Initial delay setting in +- number of ctrl_dt.step time samples of ADC or WG
ctrl_dt.step       = 0;

%% Subband FIR filter parameters
ctrl_pfir_subband.nof_polyphases= tb.subband_fft_size;
if strcmp(tb.model_filterbank, 'LOFAR')
    % Load the quantized FIR coefficients of LOFAR station
    ctrl_pfir_subband.nof_taps           = 16;                                                            % Number of taps
    ctrl_pfir_subband.nof_coefficients   = ctrl_pfir_subband.nof_polyphases*ctrl_pfir_subband.nof_taps;   % Number of filter coefficients (taps)
    ctrl_pfir_subband.overshoot_w        = 1;      % account for factor 2^overshoot_w in PFIR output quantization
    ctrl_pfir_subband.data_w             = 16;
    ctrl_pfir_subband.config.design      = 'lofar file';
    hfir_subband_coeff = load('data/Coeffs16384Kaiser-quant.dat');
else
    % Calculate free choice FIR coefficients
    ctrl_pfir_subband.nof_taps           = 16;                                                            % Number of taps
    ctrl_pfir_subband.nof_coefficients   = ctrl_pfir_subband.nof_polyphases*ctrl_pfir_subband.nof_taps;   % Number of filter coefficients (taps)
    if strcmp(tb.model_quantization, 'floating point')
        ctrl_pfir_subband.coeff_w        = 0;
        ctrl_pfir_subband.data_w         = 0;
    else
        ctrl_pfir_subband.coeff_w        = 16;     % bits/coefficient = 1 sign + (w-1) mantissa
        ctrl_pfir_subband.overshoot_w    = 1;      % account for factor 2^overshoot_w in PFIR output quantization
        ctrl_pfir_subband.data_w         = 16;
    end
    ctrl_pfir_subband.config.design      = 'fir1';
    ctrl_pfir_subband.config.design      = 'fircls1';   % 'fir1', 'fircls1'
    ctrl_pfir_subband.config.design_flag = '';          % only for fircls1: 'trace', ''
    ctrl_pfir_subband.config.interpolate = 'interpft';  % only for fircls1: 'resample', 'fourier', 'interpft'
    ctrl_pfir_subband.r_pass             = 1e-3;        % only for fircls1
    ctrl_pfir_subband.r_stop             = 1e-4;        % only for fircls1
    ctrl_pfir_subband.hp_factor          = 1.050;                                               % Adjust channel half power bandwidth
    ctrl_pfir_subband.hp_factor          = 1;
    ctrl_pfir_subband.BWchan             = ctrl_pfir_subband.hp_factor / tb.subband_fft_size;   % Channel bandwidth
    hfir_subband_coeff = pfir_coeff(ctrl_pfir_subband.nof_polyphases, ...
                                    ctrl_pfir_subband.nof_taps, ...
                                    ctrl_pfir_subband.BWchan, ...
                                    ctrl_pfir_subband.r_pass, ...
                                    ctrl_pfir_subband.r_stop, ...
                                    ctrl_pfir_subband.coeff_w, ...
                                    ctrl_pfir_subband.config);
end

ctrl_pfir_subband.coeff = reshape(hfir_subband_coeff, ctrl_pfir_subband.nof_polyphases, ctrl_pfir_subband.nof_taps);
ctrl_pfir_subband.coeff = flipud(ctrl_pfir_subband.coeff);
ctrl_pfir_subband.Zdelays = zeros(ctrl_pfir_subband.nof_polyphases, ctrl_pfir_subband.nof_taps-1);
ctrl_pfir_subband.gain = sum(ctrl_pfir_subband.coeff(:)) / ctrl_pfir_subband.nof_polyphases;
ctrl_pfir_subband.bypass = 0;

disp(sprintf(['Subband FIR filter settings:\n', ...
              '. Bypass                 = %d\n', ...
              '. Number of polyphases   = %d\n', ...
              '. Number of taps         = %d\n'], ...
              ctrl_pfir_subband.bypass, ctrl_pfir_subband.nof_polyphases, ctrl_pfir_subband.nof_taps))

%% Subband FFT parameters
ctrl_pfft_subband.fft_size = tb.subband_fft_size;
ctrl_pfft_subband.complex = false;
ctrl_pfft_subband.gain = ctrl_pfft_subband.fft_size;
if strcmp(tb.model_quantization, 'floating point')
    ctrl_pfft_subband.data_w = 0;
else
    ctrl_pfft_subband.full_scale_w = 0;
    ctrl_pfft_subband.data_w = 16;
end

%% Subband select reorder parameters
ctrl_reorder_subband.select = tb.subband_i;
ctrl_reorder_subband.select = tb.subband_i + [0:2];
ctrl_reorder_subband.select = 0:tb.nof_subbands-1;
ctrl_reorder_subband.block_size = length(ctrl_reorder_subband.select);


%% Run the data path processing
t_start = cputime;
data_dt              = zeros(tb.nof_tsub, tb.subband_fft_size);
data_pfir_subband    = zeros(tb.nof_tsub, tb.subband_fft_size);
data_pfft_subband    = zeros(tb.nof_tsub, tb.nof_subbands);
data_reorder_subband = zeros(tb.nof_tsub, ctrl_reorder_subband.block_size);
for bi = 0:tb.nof_tsub-1
    bI = bi+1;
    % Timing
    if bi==0
        ctrl_bsn_source.bsn = 0;   % Start BSN source
    end
    
    % Data path (DP)
    [ctrl_wg,              block_wg]              = wg(            ctrl_wg);
    [ctrl_dt,              block_dt]              = dt(            ctrl_dt,              block_wg);
    [ctrl_bsn_source]                             = bsn_source(    ctrl_bsn_source);
    [ctrl_pfir_subband,    block_pfir_subband]    = pfir(          ctrl_pfir_subband,    block_dt);
    [                      block_pfft_subband]    = pfft(          ctrl_pfft_subband,    block_pfir_subband);
    [ctrl_reorder_subband, block_reorder_subband] = reorder_serial(ctrl_reorder_subband, block_pfft_subband);

    % Capture data at each DP interface
    data_dt(bI, :)              = block_dt;
    data_pfir_subband(bI, :)    = block_pfir_subband;
    data_pfft_subband(bI, :)    = block_pfft_subband;
    data_reorder_subband(bI, :) = block_reorder_subband;
end
t_stop = cputime;
disp(sprintf('Total processing time: %f seconds', t_stop-t_start));


%% Plot data path results
xfig = 300;
yfig = 200;
xfigw = 1000;
yfigw = 800;
dfig = 20;

ts       = (0:tb.subband_fft_size*tb.nof_tsub-1)/tb.subband_fft_size;   % time of ADC / WG samples in subband periods
tsub_all = (0:    tb.nof_subbands*tb.nof_tsub-1)/tb.nof_subbands;       % time in subband periods for block of subbands
tsub_one = (0:                    tb.nof_tsub-1);                       % time in subband periods for one subband

sub_I      = tb.subband_I      + [0: tb.nof_subbands: tb.nof_subbands*tb.nof_tsub-1];  % get Matlab indices of all subband_I
sub_Iplus1 = tb.subband_Iplus1 + [0: tb.nof_subbands: tb.nof_subbands*tb.nof_tsub-1];  % get Matlab indices of all next neighbour subband_I+1

sel_sub_I = find(ctrl_reorder_subband.select == tb.subband_i, 1); % find Matlab index of subband_i in selected subbands

%% Plot DT output
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
data = data_dt';
plot(ts, data(:))
ylim([-1.3 1.3]);
title(sprintf('Delay tracking output data (WG subband %6.3f)', tb.subband_wg));
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel('Voltage');
grid on;

%% Plot subband PFIR transfer function
h = hfir_subband_coeff;
NL = ctrl_pfir_subband.nof_coefficients;
L = ctrl_pfir_subband.nof_taps;
hf_abs = abs(fftshift(fft(h / sum(h), NL))); 
hi = 1:NL;               % coefficients index
fx = (hi - NL/2-1) / L;  % frequency axis in subband units
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
plot(fx, db(hf_abs));  % db() = 20*log10 for voltage
%xlim([-3 3]);
grid on; 
title(['Subband FIR filter transfer function for ', ctrl_pfir_subband.config.design, ' (nof taps = ', int2str(ctrl_pfir_subband.nof_taps), ')']);
xlabel('Frequency [channels]');
ylabel('Magnitude [dB]');

%% Plot subband PFIR output
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
data = data_pfir_subband';
plot(ts, data(:))
title(sprintf('Subband polyphase FIR filter output - FFT input data (WG subband %6.3f)', tb.subband_wg));
ylim([-2 2]);             % DT step when tb.subband_wg is .5 causes double range
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel('Voltage');
grid on;

%% Plot PFFT subbands spectrum and phase for all tb.nof_tsub in one plot
sub_ampl = abs(data_pfft_subband);
sub_ampl_max = max(sub_ampl(:));
sub_phase = angle(data_pfft_subband)*180/pi;
x = sub_ampl < 0.1*sub_ampl_max;
sub_phase(x) = 0;   % force phase of too small signals to 0

fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

subplot(2,1,1);
data = sub_ampl';
data = data(:);
plot(tsub_all, data, 'k', tsub_all(sub_I), data(sub_I), 'ko', tsub_all(sub_Iplus1), data(sub_Iplus1), 'kx');
title(sprintf('Subband data - amplitude  (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
xlabel(sprintf('Subbands 0:%d at time 0:%d [Tsub]', tb.nof_subbands-1, tb.nof_tsub-1));
ylabel('Voltage');
grid on;

subplot(2,1,2);
data = sub_phase';
data = data(:);
plot(tsub_all, data, 'k', tsub_all(sub_I), data(sub_I), 'ko', tsub_all(sub_Iplus1), data(sub_Iplus1), 'kx');
ylim([-180 180])
title(sprintf('Subband data - phase  (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
xlabel(sprintf('Subbands 0:%d at time 0:%d [Tsub]', tb.nof_subbands-1, tb.nof_tsub-1));
ylabel('Phase [degrees]');
grid on;

%% Plot phase step due to DT step for the subband that is set in the WG
if isempty(sel_sub_I)
    wg_sub_phase = angle(data_pfft_subband(:, tb.subband_I))*180/pi;    % use Matlab indexing if subband is not selected
else
    wg_sub_phase = angle(data_reorder_subband(:, sel_sub_I))*180/pi;    % use functional reorder subband
end
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
plot(tsub_one, wg_sub_phase, '-o')
ylim([-180 180])
title(sprintf('Subband phase for subband %d in range 0:%d', tb.subband_i, tb.nof_subbands-1));
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel('Phase [degrees]');
grid on;

%% Plot PFFT subbands spectrum and phase for all tb.nof_tsub in separate plots
sub_ampl = abs(data_pfft_subband);
sub_ampl_max = max(sub_ampl(:));
sub_phase = angle(data_pfft_subband)*180/pi;
x = sub_ampl < 0.1*sub_ampl_max;
sub_phase(x) = 0;   % force phase of too small signals to 0

fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

if tb.plot_per_block
    for bi = 0:tb.nof_tsub-1
        bI = bi+1;
        subplot(2,1,1);
        data = sub_ampl(bI, :);
        plot(0:tb.nof_subbands-1, data, 'k', tb.subband_i, data(tb.subband_I), 'ko', tb.subband_iplus1, data(tb.subband_Iplus1), 'kx');
        ylim([0 sub_ampl_max])
        title(sprintf('Subband data - amplitude  (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
        xlabel(sprintf('Subbands 0:%d at time %d [Tsub]', tb.nof_subbands-1, bi));
        ylabel('Voltage');
        grid on;
        subplot(2,1,2);
        data = sub_phase(bI, :);
        plot(0:tb.nof_subbands-1, data, 'k', tb.subband_i, data(tb.subband_I), 'ko', tb.subband_iplus1, data(tb.subband_Iplus1), 'kx');
        ylim([-180 180])
        title(sprintf('Subband data - amplitude  (o,x = subband index %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
        xlabel(sprintf('Subbands 0:%d at time %d [Tsub]', tb.nof_subbands-1, bi));
        ylabel('Phase [degrees]');
        grid on;
        drawnow;
        %pause(0.3)
    end
end
    
%% Plot subband spectrogram for all tb.nof_tsub in one plot
data = db(sub_ampl);                % no need to scale data, range is already normalized
if strcmp(tb.model_quantization, 'floating point')
    db_low = -150;
else
    db_low = -20 - floor(6.02 * ctrl_pfft_subband.data_w);
end
%db_low = floor(min(data(data~=-Inf)))
data(data<db_low) = db_low;
mymap = jet(-db_low);
%mymap(1,:) = [0 0 0];   % force black for dB(0)
colormap(mymap);
imagesc(data',[db_low 0]);
colorbar;
title(sprintf('Subband spectogram (max value = %f = %.2f dB)', sub_ampl_max, db(sub_ampl_max)));
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel(sprintf('Subbands 0:%d', tb.nof_subbands-1));

