%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: Eric Kooistra, 2016
%
% Purpose : Model data path with two polyphase filterbanks
% Description :
% * See one_pfb.m for general description of the polyphase filterbank
%   simulation.
%
% * See delay_tracking_pfb.m for description of tb.model_delay_step
%
% * Two poly phase filterbanks (PFB)
%   Model subband filterbank and channel filterbank to make fine channels
%   per subband. The corner turner operates directly on the selected
%   subbands to have time samples grouped per subband as input for the
%   channel filter. The sync interval sets the number of blocks per
%   corner turn. From subband to subband the channel filter should
%   rerun the impulse reponse tail of the previous sync interval.
%   However if the sync interval is very large, than this rerun may be
%   ignored to simplify the implementation.
%   The FIR prefilter in the PFB can be bypassed by means of the pfir
%   tb.pfir_subband_bypass or tb.pfir_channel_bypass. Without the
%   FIR prefilter the PFB becomes a FFT.
%
% * The Apertif subband beamformer is not modelled, therefore the corner
%   turn operates on the selected subbands. The channel correlator is not
%   modelled, but typically the sync interval will be equal to the 
%   integration interval of 1.024 s (= 800000 subband blocks).
clear all;
close all;
fig=0;

%% Selectable test bench settings
% Subband filterbank
%tb.model_filterbank       = 'Free choice';         % 'LOFAR' = use fixed settings of LOFAR subband filterbank, else use free choice of tb.nof_subbands and FIR coefficients
tb.model_filterbank       = 'LOFAR';
if strcmp(tb.model_filterbank, 'LOFAR')
    tb.model_quantization = 'fixed point';
    tb.nof_subbands       = 512;
    tb.nof_channels       = 64;

    tb.subband_hp_adjust = false;   % is not applicable, because original LOFAR coefficients are fixed from file
    tb.subband_dc_adjust = true;    % can be true, because the DC adjust can be applied on the original LOFAR coefficients
    
    tb.channel_hp_adjust = false;
    tb.channel_dc_adjust = true;
else
    tb.model_quantization = 'floating point';
    tb.model_quantization = 'fixed point';
    
    tb.nof_subbands = 16;
    tb.nof_channels = 16;
    tb.nof_subbands = 512;
    tb.nof_channels = 64;
    
    tb.subband_hp_adjust = false;
    tb.channel_hp_adjust = false;

    tb.subband_dc_adjust = false;
    tb.channel_dc_adjust = false;
end
% Optionally bypass the FIR prefilter in the PFB
tb.pfir_subband_bypass = 0;
tb.pfir_channel_bypass = 0;

% Channel filterbank
tb.nof_tchan_per_sync = 20;            % nof channel periods per sync interval

% Input signal
tb.model_signal = 'sinusoid';          % Use sinusoid to check the frequency response of the filterbanks
%tb.model_signal = 'square';           % Use square to create a square wave with period set via subband_wg
%tb.model_signal = 'impulse';           % Use impulse to check the two_pfb impulse response
%tb.model_signal = 'block_pulse';       % Use block pulse to check that the subband PFIR output contains the subband PFIR coefficients

% Carrier frequency
tb.subband_wg        = 0.55;           % subband range 0:tb.nof_subbands-1, the fraction sets the relative WG channel frequency within the subband
tb.subband_wg        = 3;
%tb.subband_wg        = 12;

% Model a frequency sweep for 'sinusoid'
tb.chirp             = 0;              % 0 = use fixed tb.subband_wg frequency, else increment WG frequency every block to have chirp frequency sweep
tb.chirp             = 1;
if tb.chirp
    tb.nof_tsync     = 6;              % nof sync intervals to simulate, the corner turn operates per sync interval, suitable for ctrl_wg.df = 0.02*fchan
    %tb.nof_tsync     = 120;              % nof sync intervals to simulate, the corner turn operates per sync interval, suitable for ctrl_wg.df = 0.001*fchan
else
    tb.nof_tsync     = 2;
end

% Model PFIR incrementing coefficients
tb.force_subband_pfir_coeff = 0;       % 0 = default, else force using incrementing PFIR coefficient to ease recognizing the subband PFIR output for tb.model_signal = 'block_pulse'
if strcmp(tb.model_signal, 'block_pulse')
   tb.force_subband_pfir_coeff = 1;
end

% Model subband frequency shift by half a channel
tb.model_subband_shift = 0;            % 0 = no frequency shift, else apply a frequency shift of -fchan/2 per subband to have same aliasing in low and high channel
%tb.model_subband_shift = 1;

% Model delay tracking step response
tb.model_delay_step  = 0;              % 1 = insert delay tracking delay step to verify filterbank response, else no DT step             
%tb.model_delay_step  = 1;

tb.plot_per_block    = 0;              % 1 = plot spectrum for each block in time, else skip this plot to save time
%tb.plot_per_block    = 1;


%% Derived test bench settings
tb.nof_complex       = 2;
tb.subband_i         = floor(tb.subband_wg);            % natural subband index in range 0:tb.nof_subbands-1
tb.subband_I         = tb.subband_i+1;                  % equivalent Matlab indexing for range 1:tb.nof_subbands
if tb.subband_i<tb.nof_subbands-1                       % determine index of next neighbour subband
    tb.subband_iplus1 = tb.subband_i+1;
else
    tb.subband_iplus1 = 0;                              % wrap subband index tb.nof_subbands to subband index 0
end
tb.subband_Iplus1    = tb.subband_iplus1+1;             % equivalent Matlab index for next neighbour subband in range 1:tb.nof_subbands
tb.subband_fft_size  = tb.nof_complex*tb.nof_subbands;  % subband filterbank real FFT

tb.channel_wg        = tb.subband_wg - tb.subband_i;          % use subband fraction as WG channel offset frequency within subband
tb.channel_i         = floor(tb.channel_wg*tb.nof_channels);  % natural channel index in range 0:tb.nof_channels-1
tb.channel_I         = tb.channel_i+1;                        % equivalent Matlab indexing for range 1:tb.nof_channels
tb.channel_fft_size  = tb.nof_channels;                       % channel filterbank complex FFT

tb.nof_tchan          = tb.nof_tsync*tb.nof_tchan_per_sync;
tb.nof_tsub_per_sync  = tb.nof_tchan_per_sync*tb.channel_fft_size;  % nof subband periods per sync interval, must be multiple of nof channels per subband
tb.nof_tsub           = tb.nof_tsync*tb.nof_tsub_per_sync+1;        % one extra to be able to finish output of last sync interval

fs     = 1;                                           % normalized sample frequency
Ts     = 1;                                           % normalized sample period
fsub   = fs/tb.subband_fft_size;                      % subband frequency relative to fs
Tsub   = tb.subband_fft_size/fs;                      % subband period relative to Ts
fchan  = fsub/tb.channel_fft_size;                    % channel frequency relative to fs
Tchan  = tb.channel_fft_size*tb.subband_fft_size/fs;  % channel period relative to Ts

disp(sprintf (['Test bench settings:\n', ...
               '. Model filterbank                       = %s\n', ...
               '. Model coefficients and data            = %s\n', ...
               '. Number of subbands                     = %d\n', ...
               '. Number of subband periods to simulate  = %d\n', ...
               '. Subband filterbank real FFT size       = %d\n', ...
               '. Channel filterbank complex FFT size    = %d\n', ...
               '. WG subband frequency                   = %f\n', ...
               '. WG subband index in 0:%d               = %d\n', ...
               '. WG channel index in 0:%d               = %d\n'], ...
               tb.model_filterbank, tb.model_quantization, tb.nof_subbands, tb.nof_tsub, ...
               tb.subband_fft_size, tb.channel_fft_size, ...
               tb.subband_wg, tb.nof_subbands-1, tb.subband_i, tb.nof_channels-1, tb.channel_i))


%% Waveform generator
% Common WG settings
ctrl_wg.complex = false;
ctrl_wg.overflow = 'clip';
ctrl_wg.block_nr = 0;
ctrl_wg.block_size = tb.subband_fft_size;
if strcmp(tb.model_quantization, 'floating point')
    ctrl_wg.data_w = 0;
    lsb = 1/2^8;                            % Assume 8 bit ADC to have reference for AGWN level in floating point
else
    ctrl_wg.data_w = 8;
    lsb = 1/2^ctrl_wg.data_w;
end
ctrl_wg.agwn_sigma = 2.5*lsb;
ctrl_wg.agwn_sigma = 0;                     % AGWN sigma
ctrl_wg.ampl = 1;                           % full scale is 1, ampl > 1 causes analogue clipping
if ctrl_wg.agwn_sigma>0
    ctrl_wg.ampl = 1-5*ctrl_wg.agwn_sigma;
    ctrl_wg.ampl = 0.9;
    %ctrl_wg.ampl = 1*lsb;
end
ctrl_wg.offset = 0;                         % DC offset

% Signal specific WG settings
if strcmp(tb.model_signal, 'sinusoid')
    ctrl_wg.signal = 'sinusoid';
    ctrl_wg.psk = 0;                            % no PSK modulation
    if tb.chirp
        ctrl_wg.df = 0.02*fchan;                % increment freq by df per block to create chirp, match tb.nof_tsync to view sufficient intervals
        %ctrl_wg.df = 0.001*fchan;                % increment freq by df per block to create chirp, match tb.nof_tsync to view sufficient intervals
    else
        ctrl_wg.df = 0;
    end
    if ctrl_wg.df == 0
        ctrl_wg.freq = tb.subband_wg*fsub;      % start WG at tb.subband_wg
    else
        ctrl_wg.freq = (tb.subband_i-1)*fsub;   % start WG about one subband before tb.subband_wg to account for impulse response of several tsub
    end
    ctrl_wg.phase = 0;                          % Phase offset normalized to 2pi
    if ctrl_wg.freq == 0 && ctrl_wg.df == 0
        ctrl_wg.offset = 1;                     % Force DC offset for zero Hz
    end
else
    ctrl_wg.signal = 'pulse';
    ctrl_wg.sop = 0;                            % Initial start of pulse index
    ctrl_wg.period = 512*ctrl_wg.block_size;    % Pulse period, choose multiple of ctrl_pfir_subband.nof_taps*ctrl_pfir_channel.nof_taps to have exactly repeating output spectra
    if strcmp(tb.model_signal, 'impulse')
        ctrl_wg.width = 1;                      % Pulse width for filterbank impulse response
    else  % default to 'block_pulse'
        ctrl_wg.width = ctrl_wg.block_size;     % Pulse width for PFIR coefficients
    end
    ctrl_wg.width_remaining = 0;
end

%% Delay tracking
ctrl_dt.block_size = tb.subband_fft_size;
ctrl_dt.buffer     = zeros(1, 2*ctrl_dt.block_size);
ctrl_dt.dt         = 0;       % Initial delay setting in +- number of ctrl_dt.step time samples of ADC or WG
if tb.model_delay_step
    ctrl_dt.step = 4;         % Delay step is Psub = 4 factor, for 4 samples per clock 
else
    ctrl_dt.step = 0;
end

%% Subband FIR filter parameters
ctrl_pfir_subband.nof_polyphases = tb.subband_fft_size;
if strcmp(tb.model_filterbank, 'LOFAR')
    % Load the quantized FIR coefficients of LOFAR station
    ctrl_pfir_subband.nof_taps           = 16;                                                            % Number of taps
    ctrl_pfir_subband.nof_coefficients   = ctrl_pfir_subband.nof_polyphases*ctrl_pfir_subband.nof_taps;   % Number of filter coefficients (taps)
    ctrl_pfir_subband.overshoot_w        = 1;      % account for factor 2^overshoot_w in PFIR output quantization
    ctrl_pfir_subband.data_w             = 16;
    ctrl_pfir_subband.config.hp_adjust   = tb.subband_hp_adjust;
    ctrl_pfir_subband.config.dc_adjust   = tb.subband_dc_adjust;
    if tb.subband_dc_adjust
        ctrl_pfir_subband.config.design  = 'lofar file No DC';
        hfir_subband_coeff = load('data/Coeffs16384Kaiser-quant-nodc.dat');
    else
        ctrl_pfir_subband.config.design  = 'lofar file original';
        hfir_subband_coeff = load('data/Coeffs16384Kaiser-quant.dat');
    end
else
    % Calculate free choice FIR coefficients
    ctrl_pfir_subband.nof_taps           = 16;                                                            % Number of taps
    ctrl_pfir_subband.nof_coefficients   = ctrl_pfir_subband.nof_polyphases*ctrl_pfir_subband.nof_taps;   % Number of filter coefficients (taps)
    if strcmp(tb.model_quantization, 'floating point')
        ctrl_pfir_subband.coeff_w        = 0;
        ctrl_pfir_subband.data_w         = 0;
    else
        ctrl_pfir_subband.coeff_w        = 16;     % bits/coefficient = 1 sign + (w-1) mantissa
        ctrl_pfir_subband.overshoot_w    = 1;      % account for factor 2^overshoot_w in PFIR output quantization
        ctrl_pfir_subband.data_w         = 16;
    end
    ctrl_pfir_subband.config.design      = 'fir1';
    ctrl_pfir_subband.config.design      = 'fircls1';   % 'fir1', 'fircls1'
    ctrl_pfir_subband.config.design_flag = '';          % only for fircls1: 'trace', ''
    ctrl_pfir_subband.config.interpolate = 'interpft';  % only for fircls1: 'resample', 'fourier', 'interpft'
    ctrl_pfir_subband.r_pass             = 1e-3;        % only for fircls1
    ctrl_pfir_subband.r_stop             = 1e-4;        % only for fircls1
    ctrl_pfir_subband.config.hp_factor   = 1;           % Default channel half power bandwidth
    ctrl_pfir_subband.config.hp_adjust   = tb.subband_hp_adjust;
    ctrl_pfir_subband.config.dc_adjust   = tb.subband_dc_adjust;
    ctrl_pfir_subband.BWchan             = ctrl_pfir_subband.config.hp_factor / tb.subband_fft_size;   % Channel bandwidth
    hfir_subband_coeff = pfir_coeff_adjust(ctrl_pfir_subband.nof_polyphases, ...
                                           ctrl_pfir_subband.nof_taps, ...
                                           ctrl_pfir_subband.BWchan, ...
                                           ctrl_pfir_subband.r_pass, ...
                                           ctrl_pfir_subband.r_stop, ...
                                           ctrl_pfir_subband.coeff_w, ...
                                           ctrl_pfir_subband.config);
    if tb.force_subband_pfir_coeff
        hfir_subband_coeff = 1:ctrl_pfir_subband.nof_taps*ctrl_pfir_subband.nof_polyphases;
    end
end

ctrl_pfir_subband.coeff = reshape(hfir_subband_coeff, ctrl_pfir_subband.nof_polyphases, ctrl_pfir_subband.nof_taps);
ctrl_pfir_subband.coeff = flipud(ctrl_pfir_subband.coeff);
ctrl_pfir_subband.Zdelays = zeros(ctrl_pfir_subband.nof_polyphases, ctrl_pfir_subband.nof_taps-1);
ctrl_pfir_subband.gain = sum(ctrl_pfir_subband.coeff(:)) / ctrl_pfir_subband.nof_polyphases;
if tb.force_subband_pfir_coeff
    ctrl_pfir_subband.gain = 1;
end
ctrl_pfir_subband.bypass = tb.pfir_subband_bypass;

disp(sprintf(['Subband FIR filter settings:\n', ...
              '. Design                 = %s\n', ...
              '. Bypass                 = %d\n', ...
              '. Half power adjust      = %d\n', ...
              '. DC adjust              = %d\n', ...
              '. Number of polyphases   = %d\n', ...
              '. Number of taps         = %d\n'], ...
              ctrl_pfir_subband.config.design, ctrl_pfir_subband.bypass, ctrl_pfir_subband.config.hp_adjust, ctrl_pfir_subband.config.dc_adjust, ctrl_pfir_subband.nof_polyphases, ctrl_pfir_subband.nof_taps))

%% Subband FFT parameters
ctrl_pfft_subband.fft_size = tb.subband_fft_size;
ctrl_pfft_subband.complex = false;
ctrl_pfft_subband.gain = ctrl_pfft_subband.fft_size;
if strcmp(tb.model_quantization, 'floating point')
    ctrl_pfft_subband.data_w = 0;
else
    ctrl_pfft_subband.full_scale_w = 0;
    ctrl_pfft_subband.overshoot_w = 0;
    ctrl_pfft_subband.data_w = 16;
end

%% Subband Doppler frequency shift parameters
ctrl_shift_subband.t = 0;
ctrl_shift_subband.block_period = Tsub;
if tb.model_subband_shift
    ctrl_shift_subband.shift_freq = -fchan/2;  % Apply a frequency shift per subband have same aliasing in low and high channel
else
    ctrl_shift_subband.shift_freq = 0;
end

%% Subband select reorder parameters
ctrl_reorder_subband.select = tb.subband_i;                            % select only subband_i 
ctrl_reorder_subband.select = tb.subband_i + [-1:2];                    % select small range from subband_i 
%ctrl_reorder_subband.select = tb.subband_i + [-1:7];                    % select small range from subband_i 
ctrl_reorder_subband.select = 0:tb.nof_subbands-1;                     % select all subbands
ctrl_reorder_subband.block_size = length(ctrl_reorder_subband.select);

%% Channel FIR filter parameters
ctrl_pfir_channel.nof_polyphases = tb.channel_fft_size;
ctrl_pfir_channel.nof_taps           = 8;                                                             % Number of taps
ctrl_pfir_channel.nof_coefficients   = ctrl_pfir_channel.nof_polyphases*ctrl_pfir_channel.nof_taps;   % Number of filter coefficients (taps)
if strcmp(tb.model_quantization, 'floating point')
    ctrl_pfir_channel.coeff_w        = 0;
    ctrl_pfir_channel.data_w         = 0;
else
    ctrl_pfir_channel.coeff_w        = 9;     % bits/coefficient = 1 sign + (w-1) mantissa
    ctrl_pfir_channel.overshoot_w    = 1;      % account for factor 2^overshoot_w in PFIR output quantization
    ctrl_pfir_channel.data_w         = 16;
end
ctrl_pfir_channel.config.design      = 'fir1';
ctrl_pfir_channel.config.design      = 'fircls1';   % 'fir1', 'fircls1'
ctrl_pfir_channel.config.design_flag = '';          % only for fircls1: 'trace', ''
ctrl_pfir_channel.config.interpolate = 'interpft';  % only for fircls1: 'resample', 'fourier', 'interpft'
ctrl_pfir_channel.r_pass             = 1e-3;        % only for fircls1
ctrl_pfir_channel.r_stop             = 1e-4;        % only for fircls1
ctrl_pfir_channel.config.hp_factor   = 1;           % Default channel half power bandwidth
ctrl_pfir_channel.config.hp_adjust   = tb.channel_hp_adjust;
ctrl_pfir_channel.config.dc_adjust   = tb.channel_dc_adjust;
ctrl_pfir_channel.BWchan             = ctrl_pfir_channel.config.hp_factor / tb.channel_fft_size;   % Channel bandwidth
hfir_channel_coeff = pfir_coeff_adjust(ctrl_pfir_channel.nof_polyphases, ...
                                       ctrl_pfir_channel.nof_taps, ...
                                       ctrl_pfir_channel.BWchan, ...
                                       ctrl_pfir_channel.r_pass, ...
                                       ctrl_pfir_channel.r_stop, ...
                                       ctrl_pfir_channel.coeff_w, ...
                                       ctrl_pfir_channel.config);

ctrl_pfir_channel.coeff = reshape(hfir_channel_coeff, ctrl_pfir_channel.nof_polyphases, ctrl_pfir_channel.nof_taps);
ctrl_pfir_channel.coeff = flipud(ctrl_pfir_channel.coeff);
ctrl_pfir_channel.Zdelays = zeros(ctrl_pfir_channel.nof_polyphases, ctrl_pfir_channel.nof_taps-1);
ctrl_pfir_channel.gain = sum(ctrl_pfir_channel.coeff(:)) / ctrl_pfir_channel.nof_polyphases;
ctrl_pfir_channel.bypass = tb.pfir_channel_bypass;

disp(sprintf(['Channel FIR filter settings:\n', ...
              '. Design                 = %s\n', ...
              '. Bypass                 = %d\n', ...
              '. Half power adjust      = %d\n', ...
              '. DC adjust              = %d\n', ...
              '. Number of polyphases   = %d\n', ...
              '. Number of taps         = %d\n'], ...
              ctrl_pfir_channel.config.design, ctrl_pfir_channel.bypass, ctrl_pfir_channel.config.hp_adjust, ctrl_pfir_channel.config.dc_adjust, ctrl_pfir_channel.nof_polyphases, ctrl_pfir_channel.nof_taps))

%% Channel FFT parameters
ctrl_pfft_channel.fft_size = tb.channel_fft_size;
ctrl_pfft_channel.complex = true;
ctrl_pfft_channel.gain = ctrl_pfft_channel.fft_size;
if strcmp(tb.model_quantization, 'floating point')
    ctrl_pfft_channel.data_w = 0;
else
    ctrl_pfft_channel.full_scale_w = 0;
    ctrl_pfft_channel.overshoot_w = 0;
    ctrl_pfft_channel.data_w = 16;
end

%% Subband corner turn parameters
ctrl_corner_turn.nof_block      = tb.nof_tsub_per_sync;
ctrl_corner_turn.block_size     = ctrl_reorder_subband.block_size;
ctrl_corner_turn.buffer         = zeros(ctrl_corner_turn.nof_block, ctrl_corner_turn.block_size);
ctrl_corner_turn.output_type    = 'serial';  % 'serial' or 'matrix'
ctrl_corner_turn.bI             = 1;         % block index

% The subband corner turn tail nof block depends on channel FIR impulse
% response length. Therefor these corner turn settings need to be defined
% here after the channel filter, even though the corner turn instance is
% before the channel filter.
ctrl_corner_turn.tail_nof_block = (ctrl_pfir_channel.nof_taps-1)*ctrl_pfir_channel.nof_polyphases;
%ctrl_corner_turn.tail_nof_block = 0;
ctrl_corner_turn.tail_buffer    = zeros(ctrl_corner_turn.tail_nof_block, ctrl_corner_turn.block_size);


%% Run the data path processing at subband block rate (with one tsub block per row)
t_start = cputime;

data_dt              = zeros(tb.nof_tsub, tb.subband_fft_size);
data_pfir_subband    = zeros(tb.nof_tsub, tb.subband_fft_size);
data_pfft_subband    = zeros(tb.nof_tsub, tb.nof_subbands);
data_reorder_subband = zeros(tb.nof_tsub, ctrl_reorder_subband.block_size);
data_corner_turn     = zeros(tb.nof_tsync, ctrl_corner_turn.block_size * (ctrl_corner_turn.tail_nof_block+ctrl_corner_turn.nof_block));
sI = 1;
for bi = 0:tb.nof_tsub-1
    bI = bi+1;
    % Timing
    if bi==0
        dp_sync = 0;
        ctrl_bsn_source.bsn = 0;   % Start BSN source
    else
        dp_sync = mod(ctrl_bsn_source.bsn, tb.nof_tsub_per_sync) == 0;
    end
    
    % Control
    if ctrl_bsn_source.bsn == 30
        ctrl_dt.dt = ctrl_dt.dt - ctrl_dt.step;  % Apply delay step
    end
    
    % Data path (DP)
    [ctrl_wg,              block_wg]              = wg(            ctrl_wg);
    [ctrl_dt,              block_dt]              = dt(            ctrl_dt,              block_wg);
    [ctrl_bsn_source]                             = bsn_source(    ctrl_bsn_source);
    [ctrl_pfir_subband,    block_pfir_subband]    = pfir(          ctrl_pfir_subband,    block_dt);
    [                      block_pfft_subband]    = pfft(          ctrl_pfft_subband,    block_pfir_subband);
    [ctrl_shift_subband,   block_shift_subband]   = ds(            ctrl_shift_subband,   block_pfft_subband);
    [ctrl_reorder_subband, block_reorder_subband] = reorder_serial(ctrl_reorder_subband, block_shift_subband);
    [ctrl_corner_turn,     block_corner_turn]     = corner_turn(   ctrl_corner_turn,     block_reorder_subband, dp_sync);

    % Capture data at each DP interface
    data_dt(bI, :)              = block_dt;
    data_pfir_subband(bI, :)    = block_pfir_subband;
    data_pfft_subband(bI, :)    = block_pfft_subband;
    data_reorder_subband(bI, :) = block_reorder_subband;
    if dp_sync
        data_corner_turn(sI, :) = block_corner_turn;
        sI = sI + 1;
    end
end

%% Run the data path processing at sync interval rate (with one tsync block per row)
Nchan = tb.nof_channels;                  % number of channels per subband, = tb.nof_channels = tb.channel_fft_size = 64
Ksub  = ctrl_reorder_subband.block_size;  % number of selected subbands, = 384 for 300MHz
Kchan = Ksub*Nchan;                       % total number of channels in the selected subbands, = 384*64= 24576
Lsub  = ctrl_corner_turn.tail_nof_block;  % number of subband periods in tail of previous sync interval, = 0 or (>= nof_taps-1) * FFT size in the channel FIR filter, = 7*64 = 448
Lchan = Lsub/Nchan;                       % number of channel periods in tail of previous sync interval, = 8
Msub  = tb.nof_tsub_per_sync;             % number of subband periods in corner turn sync interval, = 1.024s * 781250 = 800000
Mchan = tb.nof_tchan_per_sync;            % number of channel periods in corner turn sync interval, = Msub/tb.channel_fft_size = 800000/64 = 12500
Msync = tb.nof_tsync;                     % number of sync periods

sprintf(['Number of channels per subband:                              Nchan = %d\n', ...
         'Number of selected subbands:                                 Ksub  = %d\n', ...
         'Total number of channels in the selected subbands:           Kchan = %d\n', ...
         'Number of subband periods in tail of previous sync interval: Lsub  = %d\n', ...
         'Number of channel periods in tail of previous sync interval: Lchan = %d\n', ...
         'Number of subband periods in corner turn sync interval:      Msub  = %d\n', ...
         'Number of channel periods in corner turn sync interval:      Mchan = %d\n', ...
         'Number of sync periods:                                      Msync = %d\n'], Nchan, Ksub, Kchan, Lsub, Lchan, Msub, Mchan, Msync)

sprintf(['                Ksub *          Mchan  = %d\n', ...
         '                Ksub * (Lchan + Mchan) = %d\n', ...
         '        Nchan * Ksub * (Lchan + Mchan) = %d\n', ...
         'Msync * Nchan * Ksub * (Lchan + Mchan) = %d\n'], Ksub*Mchan, Ksub*(Lchan+Mchan), Nchan*Ksub*(Lchan+Mchan), Msync*Nchan*Ksub*(Lchan+Mchan))

data_pfir_channel = zeros(Nchan, Ksub*Mchan, Msync);
data_pfft_channel = zeros(Nchan, Ksub*Mchan, Msync);
for sI = 1:Msync
    % Data path (DP)
    % - data_corner_turn(sI, :) = subband(1)(1:Lsub,1:Msub), subband(2)(1:Lsub,1:Msub), ..., subband(Ksub)(1:Lsub,1:Msub)
    data = reshape(data_corner_turn(sI, :), Nchan, Ksub*(Lchan+Mchan));
    cJ = 1;
    for cI = 1:Ksub*(Lchan+Mchan)
        % Data path (DP)
        [ctrl_pfir_channel, block_pfir_channel] = pfir(ctrl_pfir_channel, data(:, cI)');         % input block of Nchan subband samples in time
        [                   block_pfft_channel] = pfft(ctrl_pfft_channel, block_pfir_channel);
                
        % Capture data at each DP interface, skip the response for the Lchan tail
        if (Lchan==0) || (mod(cI-1, Lchan+Mchan) >= Lchan)
            data_pfir_channel(:, cJ, sI) = block_pfir_channel;
            data_pfft_channel(:, cJ, sI) = block_pfft_channel;                                   % capture block of Nchan channel samples
            cJ = cJ+1;
        end
    end
end

t_stop = cputime;
disp(sprintf('Total processing time: %f seconds', t_stop-t_start));


%% Plot data path results
xfig = 300;
yfig = 200;
xfigw = 1000;
yfigw = 800;
dfig = 15;

ts       = (0:tb.subband_fft_size*tb.nof_tsub-1)/tb.subband_fft_size;   % time of ADC / WG samples in subband periods
tsub_all = (0:    tb.nof_subbands*tb.nof_tsub-1)/tb.nof_subbands;       % time in subband periods for block of subbands
tsub_one = (0:                    tb.nof_tsub-1);                       % time in subband periods for one subband

sub_I      = tb.subband_I      + [0: tb.nof_subbands: tb.nof_subbands*tb.nof_tsub-1];  % get Matlab indices of all subband_I
sub_Iplus1 = tb.subband_Iplus1 + [0: tb.nof_subbands: tb.nof_subbands*tb.nof_tsub-1];  % get Matlab indices of all next neighbour subband_I+1

sel_sub_I = find(ctrl_reorder_subband.select == tb.subband_i, 1); % find Matlab index of subband_i in selected subbands

tchan_all = (1: Mchan*Msync*Kchan)/Kchan;       % time in channel periods for block of selected subbands channels
tchan_one = (1: tb.nof_tchan);                  % time in channel periods for one subband

%% Plot DT output
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
data = data_dt';
plot(ts, data(:))
ylim([-1.3 1.3]);
title(sprintf('Delay tracking output data (WG subband %6.3f)', tb.subband_wg));
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel('Voltage');
grid on;

%% Plot subband PFIR transfer function
h = hfir_subband_coeff;
NL = ctrl_pfir_subband.nof_coefficients;
L = ctrl_pfir_subband.nof_taps;
hf_abs = abs(fftshift(fft(h / sum(h), NL))); 
hi = 1:NL;               % coefficients index
fx = (hi - NL/2-1) / L;  % frequency axis in subband units
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
plot(fx, db(hf_abs));  % db() = 20*log10 for voltage
%xlim([-3 3]);
grid on; 
title(['Subband FIR filter transfer function for ', ctrl_pfir_subband.config.design, ' (nof taps = ', int2str(ctrl_pfir_subband.nof_taps), ')']);
xlabel('Frequency [channels]');
ylabel('Magnitude [dB]');

%% Plot subband PFIR output
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
data = data_pfir_subband';
plot(ts, data(:))
title(sprintf('Subband polyphase FIR filter output - FFT input data (WG subband %6.3f)', tb.subband_wg));
if ~tb.force_subband_pfir_coeff
    ylim([-2 2]);             % DT step when tb.subband_wg is .5 causes double range
end
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel('Voltage');
grid on;

%% Plot PFFT subbands spectrum and phase for all tb.nof_tsub in one plot
sub_ampl = abs(data_pfft_subband);
sub_ampl_max = max(sub_ampl(:));
sub_phase = angle(data_pfft_subband)*180/pi;
x = sub_ampl < 0.1*sub_ampl_max;
sub_phase(x) = 0;   % force phase of too small signals to 0

fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
subplot(2,1,1);
data = sub_ampl';
data = data(:);
plot(tsub_all, data, 'k', tsub_all(sub_I), data(sub_I), 'ko', tsub_all(sub_Iplus1), data(sub_Iplus1), 'kx');
title(sprintf('Subband data - amplitude  (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
xlabel(sprintf('Subbands 0:%d at time 0:%d [Tsub]', tb.nof_subbands-1, tb.nof_tsub-1));
ylabel('Voltage');
grid on;
subplot(2,1,2);
data = sub_phase';
data = data(:);
plot(tsub_all, data, 'k', tsub_all(sub_I), data(sub_I), 'ko', tsub_all(sub_Iplus1), data(sub_Iplus1), 'kx');
ylim([-180 180])
title(sprintf('Subband data - amplitude  (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
xlabel(sprintf('Subbands 0:%d at time 0:%d [Tsub]', tb.nof_subbands-1, tb.nof_tsub-1));
ylabel('Phase [degrees]');
grid on;

%% Plot phase step due to DT step for the subband that is set in the WG
if isempty(sel_sub_I)
    wg_sub_phase = angle(data_pfft_subband(:, tb.subband_I))*180/pi;    % use Matlab indexing if subband is not selected
else
    wg_sub_phase = angle(data_reorder_subband(:, sel_sub_I))*180/pi;    % use functional reorder subband
end
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
plot(tsub_one, wg_sub_phase, '-o')
ylim([-180 180])
title(sprintf('Subband phase for subband %d in range 0:%d', tb.subband_i, tb.nof_subbands-1));
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel('Phase [degrees]');
grid on;

%% Plot PFFT subbands spectrum and phase for all tb.nof_tsub in separate plots
sub_ampl = abs(data_pfft_subband);
sub_ampl_max = max(sub_ampl(:));
sub_phase = angle(data_pfft_subband)*180/pi;
x = sub_ampl < 0.1*sub_ampl_max;
sub_phase(x) = 0;   % force phase of too small signals to 0

fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

if tb.plot_per_block
    for bi = 0:tb.nof_tsub-1
        bI = bi+1;
        subplot(2,1,1);
        data = sub_ampl(bI, :);
        plot(0:tb.nof_subbands-1, data, 'k', tb.subband_i, data(tb.subband_I), 'ko', tb.subband_iplus1, data(tb.subband_Iplus1), 'kx');
        ylim([0 sub_ampl_max])
        title(sprintf('Subband data - amplitude  (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
        xlabel(sprintf('Subbands 0:%d at time %d [Tsub]', tb.nof_subbands-1, bi));
        ylabel('Voltage');
        grid on;
        subplot(2,1,2);
        data = sub_phase(bI, :);
        plot(0:tb.nof_subbands-1, data, 'k', tb.subband_i, data(tb.subband_I), 'ko', tb.subband_iplus1, data(tb.subband_Iplus1), 'kx');
        ylim([-180 180])
        title(sprintf('Subband data - amplitude  (o,x = subband index %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
        xlabel(sprintf('Subbands 0:%d at time %d [Tsub]', tb.nof_subbands-1, bi));
        ylabel('Phase [degrees]');
        grid on;
        drawnow;
        %pause(0.3)
    end
end
    
%% Plot subband spectrogram for all tb.nof_tsub in one plot
data = db(sub_ampl);                % no need to scale data, range is already normalized
if strcmp(tb.model_quantization, 'floating point')
    subband_db_low = -150;
else
    subband_db_low = -20 - floor(6.02 * ctrl_pfft_subband.data_w);
end
%subband_db_low = floor(min(data(data~=-Inf)))
data(data<subband_db_low) = subband_db_low;
mymap = jet(-subband_db_low);
%mymap(1,:) = [0 0 0];   % force black for dB(0)
colormap(mymap);
imagesc(data',[subband_db_low 0]);
colorbar;
title(sprintf('Subband spectogram (max value = %f = %.2f dB)', sub_ampl_max, db(sub_ampl_max)));
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel(sprintf('Subbands 0:%d', tb.nof_subbands-1));

%% Plot corner turn subband spectrogram
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
ctI = 1;
ct_sub_data = data_corner_turn(ctI,:);   % take one sync interval
ct_sub_data = reshape(ct_sub_data, ctrl_corner_turn.tail_nof_block+ctrl_corner_turn.nof_block, ctrl_corner_turn.block_size);
if ctrl_corner_turn.tail_nof_block>0
    ct_sub_data = ct_sub_data(ctrl_corner_turn.tail_nof_block+1:ctrl_corner_turn.tail_nof_block+ctrl_corner_turn.nof_block, :);
end
ct_sub_ampl = abs(ct_sub_data);
ct_sub_ampl_max = max(ct_sub_ampl(:));
data = db(ct_sub_ampl);                % no need to scale data, range is already normalized
data(data<subband_db_low) = subband_db_low;
mymap = jet(-subband_db_low);
colormap(mymap);
imagesc(data',[subband_db_low 0]);
colorbar;
title(sprintf('Corner turn subband spectogram for %d-th sync interval (max value = %f = %.2f dB)', ctI, ct_sub_ampl_max, db(ct_sub_ampl_max)));
xlabel(sprintf('Time 0:%d [Tsub]', Msub-1));
ylabel(sprintf('%d selected subbands', Ksub));

%% Plot channel PFIR transfer function
h = hfir_channel_coeff;
NL = ctrl_pfir_channel.nof_coefficients;
L = ctrl_pfir_channel.nof_taps;
hf_abs = abs(fftshift(fft(h / sum(h), NL))); 
hi = 1:NL;               % coefficients index
fx = (hi - NL/2-1) / L;  % frequency axis in channel units
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
plot(fx, db(hf_abs));  % db() = 20*log10 for voltage
%xlim([-3 3]);
grid on; 
title(['Channel FIR filter transfer function for ', ctrl_pfir_channel.config.design, ' (nof taps = ', int2str(ctrl_pfir_channel.nof_taps), ')']);
xlabel('Frequency [channels]');
ylabel('Magnitude [dB]');

%% Plot PFFT channel spectrum and phase for all tb.nof_tchan in one plot
%  Matlab has fastest array index first, Python has fastest array index last
data = reshape(data_pfft_channel, [Nchan, Mchan, Ksub, Msync]);  % Matlab data[Nchan][Mchan][Ksub][Msync]
data = permute(data, [2,4,1,3]);                                 % Matlab data[Mchan][Msync][Nchan][Ksub]
data = reshape(data, [Mchan*Msync, Nchan*Ksub]);  % group time samples across sync intervals and group channels across subbands

chan_ampl = abs(data);
chan_ampl_max = max(chan_ampl(:));
chan_phase = angle(data)*180/pi;
x = chan_ampl < 0.1*chan_ampl_max;
chan_phase(x) = 0;   % force phase of too small signals to 0

fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
subplot(2,1,1);
data = chan_ampl';
data = data(:);
plot(tchan_all, data)
title('Channel data - amplitude');
xlabel(sprintf('%d channels (0:%d from %d selected subbands) at time 0:%d [Tchan]', Kchan, Nchan-1, Ksub, tb.nof_tchan-1));
ylabel('Voltage');
grid on;
subplot(2,1,2);
data = chan_phase';
data = data(:);
plot(tchan_all, data)
ylim([-180 180])
title('Channel data - phase');
xlabel(sprintf('%d channels (0:%d from %d selected subbands) at time 0:%d [Tchan]', Kchan, Nchan-1, Ksub, tb.nof_tchan-1));
ylabel('Phase [degrees]');
grid on;

%% Plot channel spectrogram
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
data = db(chan_ampl);                % no need to scale data, range is already normalized
if strcmp(tb.model_quantization, 'floating point')
    channel_db_low = -150;
else
    channel_db_low = -20 - floor(6.02 * ctrl_pfft_channel.data_w);
end
data(data<channel_db_low) = channel_db_low;
mymap = jet(-channel_db_low);
colormap(mymap);
imagesc(data',[channel_db_low 0]);
colorbar;
title(sprintf('Channel spectogram (max value = %f = %.2f dB)', chan_ampl_max, db(chan_ampl_max)));
xlabel(sprintf('Time 0:%d [Tchan]', tb.nof_tchan-1));
ylabel(sprintf('%d channels (0:%d from %d selected subbands)', Kchan, Nchan-1, Ksub));

if tb.model_subband_shift
    filename = sprintf('plots/channel_spectogram_nsub_%d_nchan_%d_with_fshift.jpg', tb.nof_subbands, tb.nof_channels);
else
    filename = sprintf('plots/channel_spectogram_nsub_%d_nchan_%d_no_fshift.jpg', tb.nof_subbands, tb.nof_channels);
end
print(filename, '-djpeg')
