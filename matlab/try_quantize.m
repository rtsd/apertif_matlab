%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : Try quantize.m
% Description :
%   For x_step = 0.5 then for rounding:
%   - 'half_away' only x = 0 maps to 0
%   - 'half_up' only x = -1,0 map to 0, causing a DC offset
%
%   For x_step = 0.25 then for rounding:
%   - 'half_away' only x = -1,0,1 map to 0
%   - 'half_up' only x = -2,-1,0,1 map to 0, causing a DC offset
%
%   The plots show the original input x, the wrapped output and the
%   clipped output.

clear all;
close all;
fig=0;

% User settings
nof_bits = 3;

x_period = 2^nof_bits;
x_full_scale = 2^(nof_bits-1);
x_max = x_full_scale-1;
x_step = 1;
x_step = 0.5;
%x_step = 0.25;
x = -2*x_period:x_step:2*x_period-1;

y_half_away_wrap = quantize(x, x_full_scale, nof_bits, 'half_away', 'wrap', x_full_scale);
y_half_away_clip = quantize(x, x_full_scale, nof_bits, 'half_away', 'clip', x_full_scale);
y_half_up_wrap = quantize(x, x_full_scale, nof_bits, 'half_up', 'wrap', x_full_scale);
y_half_up_clip = quantize(x, x_full_scale, nof_bits, 'half_up', 'clip', x_full_scale);

% Plot
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(x, x, 'b*', x, y_half_away_wrap, 'kx', x, y_half_away_clip, 'ro');
ylim([-x_period x_period])
title(['Quantisation with half away and ', num2str(nof_bits), ' bits'] );
grid on;

fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(x, x, 'b*', x, y_half_up_wrap, 'kx', x, y_half_up_clip, 'ro');
ylim([-x_period x_period])
title(['Quantisation with half up and ', num2str(nof_bits), ' bits'] );
grid on;
