This directory contains Matlab code for modelling DSP aspects of Apertif:  
  
- Functions are used in scripts.
- Scripts can run in Matlab
- Figure files are saved figure plots of scripts
- Data files are input read by a scripts, or output saved by a the script


1) Fringe stopping:

  fringe.m                           Script   Plot the fringe due to the varying difference in geometrical delay.
  fringe.jpg                         Figure   Fringe plot saved by fringe.m (figure in ASTRON-MEM-199)

  phase_tracking.m                   Script   Plot the phase tracking (PT) phase and phase error as function of the
                                              varying geometrical delay (see ASTRON-MEM-199).
  phase_tracking_phases.txt          Data     Input reference data (the PT piecewise linear coefficients) for HDL
                                              simulation saved by phase_tracking.m
  phase_tracking_coefficients.txt    Data     Output reference data (the PT expected phases) for HDL simulation
                                              saved by phase_tracking.m

  phase_lookup.m                     Script   Plot and save lookup table for conversion from phi to pair of re, im


2) Quantization

  try_uencode.m                      Script   Try Matlab uencode to determine optimum quantisation, better use
                                              quantize.m
  quantize.m                         Function Quantize signed input data
  try_quantize.m                     Script   Run quantize.m

  compensation_and_selection_gain.m  Script   Determine gain_w and gain value for optimum requantisation
  
  try_power_requantize.py            Python   Investigate requantization of data and auto power for e.g. Stokes I
                                              and IQUV in Arts
  apertif_arts_firmware_model.py     Python   Model fixed point signal levels in Apertif and Arts, see
                                              ../doc/ASTRON_RP_010888_Apertif_Arts_firmware_model.pdf

3) Low pass FIR filter coefficients

  See also:
  - $RADIOHDL/libraries/dsp/doc/filterbank.txt
  - $RADIOHDL/libraries/dsp/filter/src/python/ for diff* FIR filter files and recreate* FIR files from dat
    to mif/hex format

  Fsub original from LOFAR:
  - data/Coeffs16384Kaiser-quant.dat   Data     Original PFIR filter coefficients from LOFAR 1.0 subband filterbank
                                                (16 tap, 1024 point FFT), the script that created these PFIR
                                                coefficients is not available anymore (Erko looked in old
                                                repositories back to 2005 of Lofar software and Station firmware
                                                but could not find it.)
  
  - data/Coeffs16384Kaiser-quant-withdc.dat   = data/Coeffs16384Kaiser-quant.dat

  Fsub adjusted to have no DC in PFIR coefficients per polyphase
  - data/Coeffs16384Kaiser-quant-nodc.dat     = Used in Apertif Fsub obtained from data/Coeffs16384Kaiser-quant.dat
                                                using run_pfir_coeff.m -r 18333 as commited, so with application =
                                                'lofar_subband', which selects:
                                                . config.hp_adjust = false
                                                . config.dc_adjust = true

  Fsub bypass (equivalent to not having a PFIR section, useful to isolate issues between PFB and only FFT)
  - data/run_pfir_coeff_m_bypass_16taps_1024points_16b.dat

  Fsub bypass per polyphase, used to diagnose quantization and spurious issues per polyphase, but probably not
  so useful
  - data/run_pfir_coeff_m_bypass_16taps_1024points_16b_polyphase0.dat  -- only polyphase 0 has coeff != 0
  - data/run_pfir_coeff_m_bypass_16taps_1024points_16b_polyphase1.dat  -- only polyphase 1 has coeff != 0
  - data/run_pfir_coeff_m_bypass_16taps_1024points_16b_polyphase2.dat  -- only polyphase 2 has coeff != 0
  - data/run_pfir_coeff_m_bypass_16taps_1024points_16b_polyphase3.dat  -- only polyphase 3 has coeff != 0
                                                
  Fchan bypass (equivalent to not having a PFIR section, useful to isolate issues between PFB and only FFT)
  - data/run_pfir_coeff_m_bypass_8taps_32points_9b.dat = for Arts SC4: N = 32, L = 8, coeff_w =  9.
  - data/run_pfir_coeff_m_bypass_8taps_64points_9b.dat = for Apertif
                                                bypass PFIR for Fchan using ones at tap 0 and zeros at other taps.
                                                Obtained using run_pfir_coeff.m -r 18333, but with application =
                                                'test_bypass' which selects N = 64, L = 8, coeff_w =  9.

  Fchan with half power at +-fchan/2
  - data/run_pfir_coeff_m_flat_hp_fircls1_8taps_32points_9b.dat = for Arts SC4
  - data/run_pfir_coeff_m_flat_hp_fircls1_8taps_64points_9b.dat = for Apertif
                                                PFIR for Fchan in Apertif obtained using run_pfir_coeff.m -r 18333
                                                but with application = 'apertif_channel' which selects:
                                                . config.hp_adjust = true
                                                . config.dc_adjust = false

  Fchan with half power at +-fchan/2 and no DC in PFIR coefficients per polyphase
  - data/run_pfir_coeff_m_flat_hp_no_dc_fircls1_8taps_32points_9b.dat = for Arts SC4
  - data/run_pfir_coeff_m_flat_hp_no_dc_fircls1_8taps_64points_9b.dat = for Apertif
                                                PFIR for Fchan in Apertif obtained using run_pfir_coeff.m -r 18746
                                                but with application = 'apertif_channel' which for -r 18746
                                                selects:
                                                . config.hp_adjust = true
                                                . config.dc_adjust = true  (was false in -r 18333)


  FIR_LPF_ApertifCF.m                Script   Calculate FIR filter coefficients for Apertif channel filterbank
                                              using fircls1 (original by Ronald de Wild).
  FIR_LPF_ApertifCF.txt              Data     Floating point FIR coefficients saved by FIR_LPF_ApertifCF.m
  arts_fir.png                       Figure   FIR filter transfer function plot saved by FIR_LPF_ApertifCF.m

  pfir_coeff.m                       Function Compute polyphase filterbank coefficients (same purpose as
                                              FIR_LPF_ApertifCF.m but with more options)
  pfir_coeff_adjust.m                Function Compute polyphase filterbank coefficients with optionbal half power
                                              bandwidth adjustment (calls pfir_coeff.m)
  pfir_coeff_dc_adjust.m             Function Apply DC adjustment to polyphase filterbank coefficients
  run_pfir_coeff.m                   Script   Run pfir_coeff_adjust.m and pfir_coeff.m to show filter response in
                                              time and frequency domain, and optionally calls pfir_coeff_dc_adjust.m


4) FFT
  sinusoids_and_fft_frequency_bins.m Script Code on DFT and DTFT from Matlab blog on internet
  fft_sinus_noise_scaling.m          Script to investigate sinus and noise levels of FFT input and output
  try_fft.m                          Script Try DFT and DTFT
  clk_spectrum                       Script Investigate EMI of clock and random data toggling
  
  

5) Data path functions
  wg.m                               Function Waveform generator per block of data
  dt.m                               Function Delay tracking per block of data
  bsn_source.m                       Function Block Sequence Number source per block of data
  pfir.m                             Function Polyphase FIR filter per block of data
  pfft.m                             Function FFT per block of data
  ds.m                               Function Doppler frequency shift per block of data
  reorder_serial.m                   Function Select data from block of data
  corner_turn.m                      Function Collect M blocks of in_data(1:K) and then transpose [M] and [K] for
                                              corner turn


6) Polyphase filterbank
  lofar/polyphase_demo_unquantized.m Script   original by Jan Stemerdink
  
  one_pfb.m                          Script   Run data path model with subband polyphase filterbank (real input)
  delay_tracking_pfb.m               Script   Based on one_pfb.m to simulate polyphase filterbank response on a delay step
  psk_pfb.m                          Script   Based on one_pfb.m to simulate polyphase filterbank response on PSK input

  two_pfb.m                          Script   Run data path model with subband polyphase filterbank followed by a channel
                                              polyphase filterbank
  
  

7) HDL reference data
  run_pfir.m                         Script   Run the PFIR with real input to create HDL reference data (based on one_pfb.m)
  run_pfft.m                         Script   Run the PFFT with real input to create HDL reference data (based on one_pfb.m)
  run_pfb.m                          Script   Run the PFB  with real input to create HDL reference data (based on one_pfb.m)
  run_pfft_complex.m                 Script   Run the PFFT with complex input to create HDL reference data (based on one_pfb.m)
  run_pfb_complex.m                  Script   Run the PFB  with complex input to create HDL reference data (based on one_pfb.m)

8) Correlator
  correlator_null_clip_or_wrap.m     Script   Investigate impact of input nulling, clipping or wrapping on correlator output
  
9) Beamformer
  BFweights.m                        Script   BF weights for TABs ARTS (original by Stefan Wijnholds).
  beamformer_wrap_clip.m             Script   Compare wrapping or clipping of intermediate beamformer sum