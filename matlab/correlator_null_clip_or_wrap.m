%-----------------------------------------------------------------------------
%
% Copyright (C) 2017
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: Eric Kooistra, 20 apr 2017
%
% Purpose : Compare correlator input nulling, clipping or wrapping
%   
% Description :
%   Clipping seems preferrable because it does still contribute somewhat
%   constuctive to the correlation. Clipping preserves the phase
%   information (zero crossings) of the signal and a clipped signal still
%   resembles the original overflow signal.
%   Even a little bit (ampl = 1.1) of wrapping already gives seemingly
%   random output. For severe overflow (ampl > 2) the wrapping cause extra
%   zero crossings. If the wrapped signal becomes sufficiently random
%   then the contribution of wrapped parts of the signal to the
%   correlation will be close to zero.
%   Using noise input it shows that for clipped input the correlator
%   output still follows the correlator output of the full scale input
%   that is used as reference (ref).
%   Another option is to null the input in case of overflow. The 
%   correlator output can be normalized by N or by the number of non-zero
%   contributions N-cnt. The plots show that nulling and scaling yield
%   somewhat destructive correlator output.
%   Even without RFI the signal may occasionally (e.g. ampl = 1.01)
%   overflow due to the variation of the noise. For this clipping seems
%   the most graceful choice.
%
%   Conclusion: best choose input clipping for a correlator.
%
%   Remarks:
%   . This model does not investigate the effect of the channel filterbank
%     on the data.
%   . Delay tracking and fringe stopping will take care that the input 
%     b will be in phase with input a, so should we in the plots only
%     look at the correlator output for phase difference close to 0 or 
%     should we look over the whole range? Probably look at the whole
%     range, because RFI in a side lobe or little of bore sight will
%     appear with a larger phase difference at input b.
%   . Is the sinus input relevant or should we only look at the noise
%     input? Probably the sinus input is relevant, because that is a crude
%     model of RFI.
%   
% Usage:
% . Try ampl =  1.001, 1.01, 1.1, 1.3, 2.3, 5, 100 for sinus input, to
%   vary the amount of overflow
% . Note that for sinus input the correlator output plot is symmetrical
%   around 180 degrees.

clear all;
close all;
%rng(0);    % random seed for repeatable result

% Try different amplitudes
ampl = 8;  % for input rand() or sin() use > 1 for overflow
             % for input randn() ampl = sigma, so use >~ 0.3 for overflow

N = 1000;
rad = [0:N-1]*2*pi/N;
deg = [0:N-1]*360/N;

% Try noise or sinus inputs
input = ampl*randn(1,N);        % Gaussian with sigma = ampl
input = ampl*(2*rand(1,N)-1);   % Uniform full scale ampl range
input = ampl*sin(rad);          % Sinus with amplitude = ampl

% Determine correlator output for the input and the phase shifted input
% for reference, null, null_scaled, clip and wrap.
in_ref         = zeros(1,N);
in_null        = zeros(1,N);
in_null_scaled = zeros(1,N);
in_clip        = zeros(1,N);
in_wrap        = zeros(1,N);

corr_ref         = zeros(1,N);
corr_null        = zeros(1,N);
corr_null_scaled = zeros(1,N);
corr_clip        = zeros(1,N);
corr_wrap        = zeros(1,N);

for d = 0:N-1
    dI = d+1;
    cnt = 0;
    for p = 0:N-1
        pI = p+1;
        pd = mod(p+d, N);
        pdI = pd+1;
        % reference (full scale)
        in_a = input(pI);
        in_b = input(pdI);
        if d==0
            in_ref(pI) = in_a;
        end
        corr_ref(dI) = corr_ref(dI) + in_a * in_b;
        
        % null
        in_a = input(pI);
        in_b = input(pdI);
        if in_a > 1 | in_a < -1 | in_b > 1 | in_b < -1
            cnt = cnt+1;
        end
        if in_a > 1
            in_a = 0;
        end
        if in_a < -1
            in_a = 0;
        end
        if in_b > 1
            in_b = 0;
        end
        if in_b < -1
            in_b = 0;
        end
        if d==0
            in_null(pI) = in_a;
        end
        corr_null(dI) = corr_null(dI) + in_a * in_b;
        
        % clip
        in_a = input(pI);
        in_b = input(pdI);
        if in_a > 1
            in_a = 1;
        end
        if in_a < -1
            in_a = -1;
        end
        if in_b > 1
            in_b = 1;
        end
        if in_b < -1
            in_b = -1;
        end
        if d==0
            in_clip(pI) = in_a;
        end
        corr_clip(dI) = corr_clip(dI) + in_a * in_b;
        
        % wrap
        in_a = input(pI);
        in_b = input(pdI);
        while in_a > 1
            in_a = in_a - 2;
        end
        while in_a < -1
            in_a = in_a + 2;
        end
        while in_b > 1
            in_b = in_b - 2;
        end
        while in_b < -1
            in_b = in_b + 2;
        end
        if d==0
            in_wrap(pI) = in_a;
        end
        corr_wrap(dI) = corr_wrap(dI) + in_a * in_b;
    end
    if ampl > 1
        %corr_ref(dI) = corr_ref(dI)/(ampl^2);
    end
    in_null_scaled = in_null;
    corr_ref(dI)         = corr_ref(dI)/N;
    corr_null(dI)        = corr_null(dI)/N;
    corr_null_scaled(dI) = corr_null(dI) * N/(N-cnt);  % divide by 0 will yield NaN which is fine and plots nothing
    corr_clip(dI)        = corr_clip(dI)/N;
    corr_wrap(dI)        = corr_wrap(dI)/N;
end

sum_corr_ref = sum(abs(corr_ref));
disp(sprintf('. corr_ref          = %f', sum(abs(corr_ref))        /sum_corr_ref));
disp(sprintf('. corr_clip         = %f', sum(abs(corr_clip))       /sum_corr_ref));
disp(sprintf('. corr_wrap         = %f', sum(abs(corr_wrap))       /sum_corr_ref));
disp(sprintf('. corr_null         = %f', sum(abs(corr_null))       /sum_corr_ref));
disp(sprintf('. corr_null_scaled  = %f', sum(abs(corr_null_scaled))/sum_corr_ref));


%% Plot results
fig=0;
xfig = 300;
yfig = 200;
xfigw = 1000;
yfigw = 800;
dfig = 20;


%% Plot input as function of p
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
plot(deg, in_ref, 'b', deg, in_null, 'c', deg, in_null_scaled, 'm', deg, in_clip, 'g', deg, in_wrap, 'r')
legend('reference', 'null', 'null scaled', 'clip', 'wrap');
xlabel('input period [degrees]');
ylabel('input level');
xlim([0 360]);
ylim([-1.1 1.1]*ampl);
title(sprintf('Input (ampl = %5.3f)', ampl));
grid on;

%% Plot correlator output as function of d
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
plot(deg, corr_ref, 'b', deg, corr_null, 'c', deg, corr_null_scaled, 'm', deg, corr_clip, 'g', deg, corr_wrap, 'r')
legend('reference', 'null', 'null scaled', 'clip', 'wrap');
xlabel('phase difference between input a and input b [degrees]');
ylabel('correlator output');
xlim([0 360]);
title(sprintf('Correlator output (ampl = %5.3f)', ampl));
grid on;