%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : Delay tracking (DT) per block of data
% Description :
%
%   Time -->|0           |N           |2N          |
%   Input:  |pprev       |prev        |this        |
%           |ctrl.buffer              |data        |
%           |buffer                                |
%           |abcd ... xyz|abcd ... xyz|abcd ... xyz|
%
%   Output: |prev        |this        |next
%   DT= 0:               |abcd ... xyz|abcd ... xyz|
%   DT=+1:               |zabc ...  xy|zabc ...  xy|
%   DT=+2:               |yzab ...   x|yzab ...   x|
%   DT=-1:               |bcd  ... yza|bcd  ... yza|
%   DT=+2:               |cd   ... zab|cd   ... zab|
%
% Remarks:
% * With a buffer of two blocks, so 2N, the maximum DT range is -N to + N.
% * Index = Time + 1, because for Matlab index starts at 1 instead of at 0.

function [state, out_data] = dt(ctrl, in_data)

% Data processing
N = ctrl.block_size;

% Append input data to the buffer
buffer = [ctrl.buffer in_data];

% Extract output data for given DT
out_data = buffer(1 + (N:2*N-1) - ctrl.dt);

% Keep buffer for next call
state = ctrl;
state.buffer = buffer(1 + (N:3*N-1));
