%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose:
%   Plot the phase tracking (PT) phase and phase error as function of the
%   varying geometrical delay.
%
% Description:
% 1) Theory (see ASTRON-MEM-199)
%   The geometrical delay tau_g varies due to the rotation of the earth.
%   The difference in geometrical delay between two antennas causes phase
%   difference phi at the PT function:
%
%   * phi_exact = w_RF * tau_g - w_n * tau_d
%
%   The PT simulation range can be adjusted via scale:
%
%   - 'no_scale ' : The PT can track the fringe phase for rotation of the
%                   Earth
%   - 'satellite' : The PT is configured such that it can track a
%        satellite that orbits within 1.5 hours and this configuration
%        yields the phi1_step_w that is needed for the HDL implementation.
%   - 'halfday '  : the PT simulation is speeded up to be able to track
%        the fringe phase from horizon to horizon so 180 degrees or half
%        a day. This setting is useful to view the DT delay steps in 
%        combination with the PT phases. This setting also yields the
%        reference data that is suitable for verifying the HDL
%        implementation.
%
% 2) Piecewise linear approximation
%   The piecewise linear phase tracking tracks phi_exact using linear
%   interpolation during each update interval T_update:
%
%   * phi_linear  : piecewise linear intepolation double coefficients
%
% 3) Quantization
%   * phi_quant   : piecewise linear intepolation quantized coefficients
%   * phi_requant : piecewise linear intepolation requantized coefficients
%
%   The required accuracy for the PT is set via phi_error_max. Use no
%   scaling to determine the required number of bits for the PT
%   coefficients.
%
% 4) Frequency shift support
%   The phase phi0 has range 360 degrees. For geometrical delay
%   compensation the phase step phi1 can suffice with a much smaller range
%   than 360 degrees, because the maximum phi1 phase step will be much
%   less than 1 degree. However if the PT is also used to apply a
%   frequency shift within the subband, then the phase step can become
%   much larger. This phase step is represented separately by shift1,
%   because another difference between shift1 and phi1 is that the
%   frequency shift1 is the same for all subbands and all compound beams.
%   Typically f_shift = -f_sub/N_chan/2 to avoid that both -f_sub/2 and
%   +f_sub/2 alias into channel 0 of a fine channel filterbank with N_chan
%   fine channels (see ASTRON-MEM-197). The shift1 register uses range 
%   M_phi1, it is not necessary to save register space because it is a
%   single register that is applied for all f_n and all CB. Furthermore
%   using the full range M_phi1 allows defining any f_shift, so no only
%   f_shift = -fchan/2. If only -fchan/2 needs to be supported, then
%   there could be some backoff bits (dependent on N_chan) and there
%   could be less fraction bits (dependent on M_phi / N_chan).
%   Set shift1 = 0 to disable the frequency shift support function.
%
% 5) HDL reference data
%   Use scale = 'halfday  ' to create reference data for an HDL
%   implementation of the PT. With these scaled settings each piecewise
%   linear interval is shorter. The HDL reference data is saved in files.
%   Each line in the PT coefficients file contains the piecewise linear
%   coefficients for one interval and each line in the PT output phases
%   file contains the expected interpolated values for phi_rq in range
%   0:M_phi-1.
%
%   PT coefficients (dual page: one being written, one being applied):
%     hdl_shift0_q    : Frequency shift offset, zero for all t, f, CB if
%                       f_update / f_shift is integer
%     hdl_shift1_q    : Frequency shift, fixed same value for all t, f, CB
%     hdl_phi0_q      : PT phase offset, fixed per T_update, different for f, CB
%     hdl_phi1_q      : PT phase step,   fixed per T_update, different for f, CB
%
%   PT output phases:
%     hdl_phases      : Expected output phases per T_update, different for t, f, CB
% 
%   The hdl_phases are the addresses for a 0:M_phi-1 --> (Re, Im) lookup
%   table. The (Re, Im) complex values are at least Wpt = 8b and have to
%   be multiplied with the beamlets to apply the frequency shift and to
%   stop the fringe per beamlet.

clear all;
close all;

% -------------------------------------------------------------------------
% User control
% -------------------------------------------------------------------------

% - PT accuracy
phi_error_max = 0.5;        % choose maximum PT phase error in degrees
phi0_extra_w = 0;           % no extra bits are needed to achieve phi_error_max, however use e.g. 8 extra bits to see phi_quant approach phi_linear
%phi0_extra_w = 8;
nof_N_int = 1;             % default 1 for T_update interval equal to T_int, use > 1 to try effect on phi_error_max of longer update intervals
hdl_N_int = 1;              % update interval as assumed for dimensioning the PT function, choose nof_N_int > hdl_N_int to check what
                            % happens when the update rate is less than designed for in HDL

% - Optional PT frequency shift
N_chan = 64;                % number of fine channels per subband
N_shift = N_chan*2;         % choose frequency shift relative to the subband frequency, typically -f_chan/2 to avoid channel 0 aliasing at f_sub/2
shift1 = -360 / N_shift;    % shift frequency phase step shift1/360 = f_shift/f_sub, choose negative to represent frequency shift -f_chan/2
shift1 = 0;                 % no frequency shift

% - Initial observation angle theta
g_theta0 = -90;             % choose start observation angle in degrees (-90 is east, 0 is up, +90 is west)
%g_theta0 = 0;

% - Optional scaling 
scale = 'no_scale ';        % choose this to get parameter widths for PT that can track the earth rotation (24h circle)
%scale = 'satellite';        % choose this to get parameter widths for PT that can track an orbiting satellite (1.5 hour orbit)
%scale = 'halfday  ';        % choose this to speed up the simulation of PT over half a day (theta0 = -90 to +90 degrees), this data is suitable for HDL reference

% -------------------------------------------------------------------------
% Model
% -------------------------------------------------------------------------

% Scaling
scale_w_e = 1;              % default no scaling
scale_T_update = 1;
scale_f = 1;
scale_B = 1;
if scale == 'halfday  '
    scale_w_e = 10000;      % speed up earth rotation to effectively make shorter day
    scale_T_update = scale_w_e;
    scale_f = 10;           % decrease f_RF to have larger T_sub unit time steps
    scale_B = 10;           % increase B to compensate for scale_f and preserve sufficient number of delay tracking DT steps Td
elseif scale == 'satellite'
    scale_w_e = 16;         % speed up earth rotation to effectively model the angular speed of a satellite (e.g. 24 h / 16 = 1.5 hour orbit)
    scale_T_update = 1;     % do no compensate for increased scale_w_e, to check the impact on the PT accuracy
end

% Physical constants
c = 3e8;                    % speed of light
w_e = 7.3e-5 * scale_w_e;   % angluar rotation speed of the Earth

% WSRT constants
B = 3000 / scale_B;         % baseline length
d0_max = B/c;

% Apertif constants
fs = 800e6 / scale_f;
Ts = 1/fs;
RF_BW = fs / 2;    % critically sampled
N_FFT = 1024;
N_sub = N_FFT/2;
f_sub = RF_BW / N_sub;
T_sub = 1/f_sub;
f_shift = f_sub * shift1/360;
f_low = 1400e6 / scale_f;
P_sub = 4;
Td = P_sub * Ts;
k_max = ceil(d0_max / Td);

% Subband frequency
fi = 448;                % choose subband index from 0:N_sub-1
f_n = fi * f_sub;        % subband periods / s
w_n = 2*pi*f_n;          % subband radials / s
g_n = 360*f_n;           % subband degrees / s
f_RF = f_low + f_n;      % RF subband periods / s
w_RF = 2*pi*f_RF;        % RF subband radials / s
g_RF = 360*f_RF;         % RF subband degrees / s

f_fringe_max = f_RF * w_e * B / c;
T_fine = 0.01 / f_fringe_max;
T_linear = sqrt(T_fine / w_e);

% Time axis
N_int = 800000;
N_update = N_int * nof_N_int;
T_update = N_update / f_sub / scale_T_update;   % choose update interval equal to number of integration intervals
nof_t_sub = round(N_update / scale_T_update);   % number of subband time samples per update interval
hdl_t_sub = round(N_update * hdl_N_int / nof_N_int / scale_T_update);   % number of subband time samples per update interval as designed for in HDL
t_i = (0:nof_t_sub-1);
t_u = t_i/f_sub;

if T_linear < T_update
    sprintf('Warning: the update interval is too long for piecewise linear interpolation (T_linear = %5.3f s while T_update = %5.3f s) !!!\n', T_linear, T_update)
end

% Duration of the simulation
T_halfday = 12 * 3600 / scale_w_e;          % time in s
T_halfday_sub = T_halfday / T_sub;          % time in T_sub

if T_halfday_sub > 1e6  
    nof_update = 20;                               % choose number of PT update intervals
else
    nof_update = ceil(T_halfday_sub / nof_t_sub);  % simulate PT for half day if feasible in time and in computer memory
end
T_sim_sub = nof_update * nof_t_sub;
T_sim = nof_update * T_update;

% PT phase and number of bits:
%                             <-- 360 degrees --->
%   output phase phi        : <-- phi_w --------->
%
%   input phase offset phi0 : <-- phi_w ---------><-- phi0_extra_w ----->
%                             |----- M_phi ------||----- M_phi0_extra --|
%                             <-- phi0_w ------------------------------->
%                             |----- M_phi0 ----------------------------|
%
%   input phase step   phi1 : <-- phi_w ---------><-- phi0_extra_w --><-- phi1_interpolate_w -->
%                                                 <-- phi1_fraction_w ------------------------->
%                             |----- M_phi ------||----- M_phi1_fraction ----------------------|
%                             <-- phi1_w ------------------------------------------------------>
%                             |----- M_phi1 ---------------------------------------------------|
%                             <-- phi1_backoff_w ---------><-- phi1_step_w -------------------->
%                                                          |----- M_phi1_step -----------------|
%
%   input frequency offset shift0 (via phi0).
%   input frequency step   shift1 (via dedicated shift1 coefficient with range M_phi1):
%                             <-- phi1_w ------------------------------------------------------>
%                             |----- M_phi1 ---------------------------------------------------|
%                             |----- M_phi ------||----- M_phi1_fraction ----------------------|

% - output phase phi
phi_w = ceil(log2(360/phi_error_max));        % width _w is nof bits to represent 0:360, used for phi -> (Re,Im) look up table address range in HDL
M_phi = 2^phi_w;                              % range M_ is 2^_w, range [0:M_phi-1] maps to [0:2*pi>

% - input phase offset coefficient phi0
phi0_w = phi_w + phi0_extra_w;                % width _w is nof bits
M_phi0_extra = 2^phi0_extra_w;                % range M_ is 2^_w
M_phi0 = 2^phi0_w;
phi0_error_max = 360 / M_phi0;

% - input phase step coefficient phi1
d0_max = B / c;
d1_max = w_e * d0_max;
phi1_max = g_RF * d1_max * T_sub;
phi1_interpolate_w = ceil(log2(hdl_t_sub/phi_error_max));   % nof bits to preserve phi_error_max after adding nof_t_sub
phi1_fraction_w = phi0_extra_w + phi1_interpolate_w;        % nof bits to preserve phi_error_max after adding nof_t_sub and phi0_extra_w 
phi1_w = phi_w + phi1_fraction_w;                           % width _w is nof bits
phi1_backoff_w = floor(log2(360 / phi1_max)) - 1;           % -1 for sign bit of phase step phi1
phi1_step_w = phi1_w - phi1_backoff_w;
M_phi1_fraction = 2^phi1_fraction_w;
M_phi1 = 2^phi1_w;
M_phi1_step = 2^phi1_step_w;

% - input frequency phase shift1
if f_shift ~= 0
    if abs(f_sub/f_shift - round(f_sub/f_shift))> 0.001/hdl_t_sub
        sprintf('Note: f_sub should be an integer multiple of f_shift, else hdl_phi0_q must adjust for shift0_q as well !!!\n')
    end
    if mod(hdl_t_sub, N_shift) > 0.001/hdl_t_sub
        sprintf('Note: T_update should be an integer multiple of T_shift, else hdl_phi0_q must adjust for shift0_q as well !!!\n')
    end
end
shift1_w = phi1_w;
N_shift1 = 2^shift1_w;

% Echo settings
sprintf (['Scale up factor for earth rotation rate            = %d\n', ...
          'Scale down factor for update interval              = %d\n', ...
          'Scale down factor for RF frequencies               = %d\n', ...
          'Scale up factor for baseline length                = %d\n', ...
          'Scaled observation time for half day (12 hours)    = %7.2f [s]\n', ...
          'Simulated observation time                         = %7.2f [s]\n', ...
          'Subband RF frequency                               = %7.3f [Hz]\n', ...
          'Subband frequency                                  = %9.1f [Hz]\n', ...
          'Frequency shift                                    = %9.3f [Hz]\n', ...
          'DT delay step Td                                   = %7.3f [ns]\n', ...
          'Maximum nof delay steps Td                         = %d\n', ...
          'Maximum fringe frequency                           = %7.2f [Hz]\n', ...
          'Maximum linear update interval                     = %7.5f [s]\n', ...
          'Linear update interval                             = %7.5f [s]\n', ...
          'Nof update intervals                               = %d\n', ...
          'Nof subband time samples per update interval       = %d\n', ...
          'Nof subband time samples per update interval HDL   = %d\n', ...
          'Nof subband time samples in total simulation       = %d\n', ...
          'Frequency shift step shift1                        = %f [degrees]\n', ...
          'Maximum phase step phi1                            = %f [degrees]\n', ...
          'Maximum PT phase error specified                   = %7.3f [degrees]\n', ...
          'Maximum PT phase error HDL                         = %7.3f [degrees]\n', ...
          'Nof bits for 360 degrees input phase phi0          = %d\n', ...
          'Nof bits for input interpolation interval          = %d\n', ...
          'Nof bits for input phase step phi1                 = %d\n', ...
          'Nof bits for input frequency shift phase shift1    = %d\n', ...
          'Nof bits for 360 degrees internal phase            = %d\n', ...
          'Nof bits for 360 degrees internal phase fraction   = %d\n', ...
          'Nof bits for 360 degrees output phase phi          = %d\n'], ...
          scale_w_e, scale_T_update, scale_f, scale_B, T_halfday, ...
          T_sim, f_RF/1e6 , f_sub, f_shift, Td*1e9, k_max, ...
          f_fringe_max , T_linear, T_update, ...
          nof_update, nof_t_sub, hdl_t_sub, T_sim_sub, ...
          shift1, phi1_max, phi_error_max, phi0_error_max, ...
          phi0_w, phi1_interpolate_w, phi1_step_w, phi1_w, phi1_w, phi1_fraction_w, phi_w) 

% Simulate the phase tracking (PT)
r_theta0 = g_theta0 * pi/180;
DT = [];
phi_exact = [];
phi_linear = [];
phi_quant = [];
phi_requant = [];
hdl_coefficients = [];
hdl_phases = [];
for up = 0:nof_update-1
    % Exact phase
    g_shift_begin = 360*f_shift * T_update * up;
    g_shift_exact = g_shift_begin + 360*f_shift * t_u;
    
    theta_begin = r_theta0 + w_e * T_update * up;
    theta = theta_begin + w_e * t_u;
    tau_g_exact = sin(theta) * B / c;
    tau_g_begin = tau_g_exact(1);
    
    k = round(tau_g_begin / Td);
    tau_d = k * Td;
    DT = [DT k];
    
    phi_exact = [phi_exact g_shift_exact + g_RF * tau_g_exact - g_n * tau_d];
    
    % Piecewise linear phase
    shift_linear = g_shift_begin + shift1 * t_i;
    
    d0 = tau_g_begin;
    d1 = cos(theta_begin) * w_e * B / c;
    
    tau_g_linear = d0 + d1 * t_u;
    phi_linear = [phi_linear shift_linear + g_RF * tau_g_linear - g_n* tau_d];
    
    % Quantized piecewise linear phase
    % - quantize the PT coefficients
    shift0_q = round(M_phi0 * g_shift_begin/360);        % if shift0_q is not 0, then it needs to be adjusted via phi0, which has range M_phi0
    shift1_q = round(M_phi1 * shift1/360);               % for shift1_q use full M_phi1 range, to support any f_shift
    phi0 = g_RF * d0 - g_n * tau_d;
    phi0_q = round(M_phi0 * phi0/360);
    phi1 = g_RF * d1 * T_sub;
    phi1_q = round(M_phi1 * phi1/360);
    
    % - requantize the PT output
    phi_q = (shift0_q + phi0_q) / M_phi0_extra + (shift1_q + phi1_q) * t_i / M_phi1_fraction;  % quantized real output phi_q
    phi_quant = [phi_quant phi_q * 360/M_phi];           % use real phi_q output to see effect of increasing phi0_extra_w
    phi_rq = round(phi_q);                               % requantize real phi_q output to get integer output phi_rq
    phi_requant = [phi_requant phi_rq * 360/M_phi];      % use integer phi_rq output to see actual requantized output
    
    % Keep reference data for HDL simulation
    % - input coefficients per update interval
    hdl_shift0_q = mod(shift0_q, M_phi0);     % phase offset coefficient modulo 360 degrees, use modulo to have natural range 0:360
    hdl_shift1_q = shift1_q;                  % use full internal M_phi1 range for frequency shift (same for all w_n and all CB pointings)
    hdl_phi0_q = mod(phi0_q, M_phi0);         % phase offset coefficient modulo 360 degrees, use modulo to have natural range 0:360
    hdl_phi1_q = rem(phi1_q, M_phi1_step);    % phase step   coefficient modulo maximum phase step, use rem to preserve sign
    hdl_coefficients = [hdl_coefficients; [hdl_shift0_q hdl_shift1_q hdl_phi0_q hdl_phi1_q]];
    
    % - expected output phases per update interval
    hdl_phi_rq = mod(phi_rq, M_phi);          % output phase modulo 360 degrees, use modulo to have natural range 0:M_phi
    hdl_phases = [hdl_phases; hdl_phi_rq];
end
g_theta_end = (r_theta0 + w_e * T_update * nof_update) * 180/pi;

% Save PT input and output reference data for HDL simulation
file_name = ['phase_tracking_coefficients.txt'];
fid       = fopen(file_name, 'w');
[N, M] = size(hdl_coefficients);
for I=1:N
    fprintf(fid, '%20d', hdl_coefficients(I,:));
    fprintf(fid, '\n');
end
fclose(fid);

file_name = ['phase_tracking_phases.txt'];
fid       = fopen(file_name, 'w');
[N, M] = size(hdl_phases);
for I=1:N
    fprintf(fid, '%5d', hdl_phases(I,1:M));
    fprintf(fid, '\n');
end
fclose(fid);

% Plot PT results
tPhi = (0:nof_update*nof_t_sub-1)/f_sub/T_update;
tTd  = 0:nof_update-1;

fig=0;
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(tPhi, phi_exact)
title(['Phi exact, for theta = ' num2str(g_theta0) ' to ' num2str(g_theta_end) ' [degrees]']);
xlabel(['Update intervals, T update = ' num2str(T_update) ' [s]']);
ylabel(['Phase [degrees]']);
grid on;

fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(tTd, DT, 'o')
title(['Delay tracking, for theta = ' num2str(g_theta0) ' to ' num2str(g_theta_end) ' [degrees]']);
xlabel(['Update intervals, T update = ' num2str(T_update) ' [s]']);
ylabel(['DT steps']);
grid on;

fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(tPhi, phi_exact-phi_linear)
%axis([0 nof_update -phi_error_max phi_error_max]);
title(['Phi exact - phi linear, for theta = ' num2str(g_theta0) ' to ' num2str(g_theta_end) ' [degrees]']);
xlabel(['Update intervals, T update = ' num2str(T_update) ' [s]']);
ylabel(['Phase error [degrees]']);
grid on;

fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(tPhi, phi_exact-phi_quant)
%axis([0 nof_update -phi_error_max phi_error_max]);
title(['Phi exact - phi quant, for theta = ' num2str(g_theta0) ' to ' num2str(g_theta_end) ' [degrees]']);
xlabel(['Update intervals, T update = ' num2str(T_update) ' [s]']);
ylabel(['Phase error [degrees]']);
grid on;

fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(tPhi, phi_exact-phi_requant)
%axis([0 nof_update -phi_error_max phi_error_max]);
title(['Phi exact - phi requant, for theta = ' num2str(g_theta0) ' to ' num2str(g_theta_end) ' [degrees]']);
xlabel(['Update intervals, T update = ' num2str(T_update) ' [s]']);
ylabel(['Phase error [degrees]']);
grid on;
