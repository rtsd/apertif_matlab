%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: Eric Kooistra, 2016
% Purpose : Try FFT and DTFT based on sinusoids_and_fft_frequency_bins.m
clear all;
close all;

%% Discrete FT
Nfft = 64;             % DFT size
t = 0:Nfft-1;          % number of samples equalt to DFT size
fs = 1;                % normalized sample frequency
fbin = fs/Nfft;        % bin frequency relative to fs

freq = 13.5 * fbin;
a = sin(2*pi*freq*t);
figure(1)
plot(t,a)
grid on;

A = fft(a, Nfft);      % A = DFT(a)
figure(2)
f = 0:Nfft-1;
plot(f, abs(A), '*')
grid on;
hold on;

%% Discrete Time FT
Nf = 16;                 % Factor DFT - DTFT
Ndtft = Nfft*Nf;         % DTFT size

AA = fft(a, Ndtft);    % AA = DTFT(a)
ff = (0:Ndtft-1)/Nf;
plot(ff, abs(AA))

