%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose:
%   Plot the fringe due to the varying difference in geometrical delay.
% Description:
%   The geometrical delay tau_g varies due to the rotation of the earth.
%   The difference in geometrical delay between two antennas causes phase
%   difference phi at f_RF. Plot the fringe pattern of cos(phi):
%
%     cos(phi(t)) = cos(w_RF*tau_g(t)) = cos(w_RF B / c * sin(theta))
%
clear all;
close all;

% Physical constants
c = 3e8;           % speed of light
w_e = 7.3e-5;      % angluar rotation speed of the Earth

% WSRT constants
scale = 1000;      % scale factor to make the plot
B = 3000/scale;    % baseline length
f_RF = 1750e6;     % center frequeny at RF
w_RF = 2*pi*f_RF;  % angular frequeny at RF 
L_RF = c/f_RF;     % wave length

% Time axis
t_hours = 12;      % one half earth rotation from east to west 
t_seconds = t_hours*3600;
t_res = 0.01;
t = (-t_seconds/2:t_res:t_seconds/2);
theta = w_e*t;     % angle between wavefront and baseline

% Fringe phase
d = B*sin(theta);  % geometrical delay in meters
tau_g = d/c;       % geometrical delay in seconds
phi = w_RF*tau_g;  % geometrical delay phase at RF

% Plot fringe
plot(theta*180/pi, cos(phi));
axis([-max(theta)*180/pi max(theta)*180/pi -1.1 1.1]);
grid on;
title(['Fringes due to rotation of Earth (f_R_F = ' num2str(f_RF/1e9) ' GHz, B = ' num2str(B) ' m)']);
xlabel('theta in deg');
ylabel(['fringe amplitude']);
print -djpeg plots/fringe.jpg

% maximum fringe frequency at theta=0, so first period after t=0
f_fringe_max = scale * w_e / asin(L_RF / B);
disp(sprintf('f_fringe_max = %5.3f Hz\n', f_fringe_max));
