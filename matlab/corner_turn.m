%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : Collect M blocks of in_data(1:K) and then transpose [M] and [K].
% Description :
%
%   The corner turn can also include the last L blocks from the previous
%   interval, so then tranpsose [L+M] and [K].
%
%   M=nof_block
%   K=block_size
%   L=tail_nof_block
%
%   Tail previous buffer at in_sync:
%
%     M-L+1   in_data(1:K)
%     M-L+2   in_data(1:K)
%     ...
%     M-L+L   in_data(1:K)
%
%   Buffer at in_sync (i.e. when bI == 1):
%     1   in_data(1:K)
%     2   in_data(1:K)
%     ...
%     M   in_data(1:K)
%
%   Output at in_sync when output_type = 'matrix':
%
%     1   in_data(1)(1:L,1:M)
%     2   in_data(2)(1:L,1:M)
%     ...
%     K   in_data(K)(1:L,1:M)
%
%   Output at in_sync when output_type = 'serial':
%
%     in_data(1)(1:L,1:M),in_data(2)(1:L,1:M), ..., in_data(K)(1:L,1:M)
%
%   The in_data blocks are collected into the buffer. The out_data is the
%   transposed buffer and output at the in_sync. The out_data is
%   optionally prependend with L blocks from the previous buffer.
%   Typically these L tail blocks can be used to reinitialize the delay
%   elements in a FIR filter. The sync interval should typically match M
%   in_data blocks.
%
function [state, out_data] = corner_turn(ctrl, in_data, in_sync)

L = ctrl.tail_nof_block;
M = ctrl.nof_block;
K = ctrl.block_size;

% Keep state for next call
state = ctrl;

% Corner turn output data at in_sync
out_data = [];
if in_sync
    if ctrl.tail_nof_block==0
        out_data = state.buffer;
    else
        out_data = [state.tail_buffer; state.buffer];  % prepend tail blocks from previous buffer
    end
    if strcmp(ctrl.output_type, 'serial')
        out_data = out_data(:)';  % transpose output into 1 row for serial transport
    else
        out_data = out_data.';    % transpose output into K rows for processing
    end
    
    % Start new buffer
    state.tail_buffer = state.buffer(M-L+1:M, :);  % preserve tail
    state.bI = 1;
end

% Collect input data
state.buffer(state.bI, :) = in_data;  % Store new input data

% Next bI
state.bI = ctrl.bI + 1;
if state.bI>M
    state.bI = 1;   % missed in_sync, no output, just continue
end
