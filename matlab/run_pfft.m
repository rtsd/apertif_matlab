%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: Eric Kooistra, 2016 (for Apertif)
%
% Purpose : Run the PFFT with real input:
%   To create input and output reference data for HDL implementation in
%   data/, e.g.:
%
%    . run_pfft_m_impulse_chirp_8b_128points_16b.dat
%
% Description :
%
% * See one_pfb.m for general description. This run_pfft.m is similar but
%   without the PFIR, so only the WG and the PFFT.
clear all;
close all;
rng('default');
fig=0;

%% Selectable test bench settings
% Input signal
tb.model_signal = 'sinusoid';          % Use sinusoid to check the frequency response of the FFT with frequency set via tb.subband_wg
%tb.model_signal = 'noise';              % Use noise to check all aspects in one signal
%tb.model_signal = 'square';            % Use square to create a square wave with period set via tb.subband_wg
%tb.model_signal = 'impulse';           % Use impulse to check the impulse response for different ctrl_wg.sop 

tb.model_quantization = 'floating point';
tb.model_quantization = 'fixed point';
tb.nof_subbands = 16;

% Carrier frequency
tb.subband_wg        = 1;           % subband range 0:tb.nof_subbands-1, can be fraction to have any sinusoid frequency
if tb.nof_subbands == 64
    tb.subband_wg = 43;             % use 43 to recreate HDL reference data for nof_subbands = 64, else use 1 to fit any nof_subbands
end
%tb.subband_wg        = 1.55;
%tb.subband_wg        = 12;

tb.phase             = 0;           % initial 'sinusoid' phase offset normalized to 2pi
tb.sop               = 1;           % initial 'impulse' start index in range ctrl_wg.block_size = tb.subband_fft_size

% Model a frequency sweep of the 'sinusoid'
tb.chirp             = 0;              % 0 = use fixed tb.subband_wg frequency or pulse period equal to block_size
tb.chirp             = 1;              % else increment WG frequency every block to have chirp frequency sweep or slide the pulse
if strcmp(tb.model_signal, 'noise')
    tb.nof_tsub      = 10 ;            % number of subband periods to simulate
elseif tb.chirp
    tb.nof_tsub      = 200;            % number of subband periods to simulate
else
    tb.nof_tsub      = 5;            % number of subband periods to simulate
end

tb.plot_per_block    = 0;              % 1 = plot spectrum for each block in time, else skip this plot to save time
%tb.plot_per_block    = 1;


%% Derived test bench settings
tb.nof_complex       = 2;
tb.subband_i         = floor(tb.subband_wg);            % natural subband index in range 0:tb.nof_subbands-1
tb.subband_I         = tb.subband_i+1;                  % equivalent Matlab indexing for range 1:tb.nof_subbands
if tb.subband_i<tb.nof_subbands-1                       % determine index of next neighbour subband
    tb.subband_iplus1 = tb.subband_i+1;
else
    tb.subband_iplus1 = 0;                              % wrap subband index tb.nof_subbands to subband index 0
end
tb.subband_Iplus1    = tb.subband_iplus1+1;             % equivalent Matlab index for next neighbour subband in range 1:tb.nof_subbands
tb.subband_fft_size  = tb.nof_complex*tb.nof_subbands;  % subband real FFT

fs                   = 1;                       % normalized sample frequency
fsub                 = fs/tb.subband_fft_size;  % subband frequency relative to fs

disp(sprintf (['Test bench settings:\n', ...
               '. Model coefficients and data            = %s\n', ...
               '. Number of subbands                     = %d\n', ...
               '. Number of subband periods to simulate  = %d\n', ...
               '. Subband real FFT size                  = %d\n', ...
               '. WG input signal                        = %s\n', ...
               '. WG subband frequency                   = %f\n'], ...
               tb.model_quantization, tb.nof_subbands, tb.nof_tsub, tb.subband_fft_size, tb.model_signal, tb.subband_wg))


%% Waveform generator
% Common WG settings
ctrl_wg.complex = false;
ctrl_wg.overflow = 'clip';
ctrl_wg.block_nr = 0;
ctrl_wg.block_size = tb.subband_fft_size;
if strcmp(tb.model_quantization, 'floating point')
    ctrl_wg.data_w = 0;
    lsb = 1/2^8;                            % Assume 8 bit ADC to have reference for AGWN level in floating point
else
    ctrl_wg.data_w = 8;
    ctrl_wg.q_full_scale = 2^(ctrl_wg.data_w-1);
    lsb = 1/2^ctrl_wg.data_w;
end
ctrl_wg.agwn_sigma = 2.5*lsb;
ctrl_wg.agwn_sigma = 0;                     % AGWN sigma
ctrl_wg.ampl = 1;                           % full scale is 1, ampl > 1 causes analogue clipping
if ctrl_wg.agwn_sigma>0
    ctrl_wg.ampl = 1-5*ctrl_wg.agwn_sigma;
    ctrl_wg.ampl = 0.9;
    %ctrl_wg.ampl = 1*lsb;
end
ctrl_wg.offset = 0;                         % DC offset
%ctrl_wg.offset = 0.1;

% Signal specific WG settings
if strcmp(tb.model_signal, 'sinusoid')
    ctrl_wg.signal = 'sinusoid';
    ctrl_wg.psk = 0;                            % no PSK modulation
    if tb.chirp
        ctrl_wg.df = 0.49*fsub;                % increment freq by df per block to create chirp
        %ctrl_wg.df = 0.5*fsub;
    else
        ctrl_wg.df = 0;
    end
    if ctrl_wg.df == 0
        ctrl_wg.freq = tb.subband_wg*fsub;      % start WG at tb.subband_wg
    else
        ctrl_wg.freq = (tb.subband_i-1)*fsub;   % start WG about one subband before tb.subband_wg to account for impulse response of several tsub
    end
    ctrl_wg.phase = tb.phase;                   % Phase offset normalized to 2pi
    if ctrl_wg.freq == 0 && ctrl_wg.df == 0
        ctrl_wg.offset = 1;                     % Force DC offset for zero Hz
    end
elseif strcmp(tb.model_signal, 'noise')
    ctrl_wg.signal = 'noise';
    ctrl_wg.ampl = 0;
    ctrl_wg.agwn_sigma = 1/3;                   % AGWN sigma is 1/3 full scale
else
    ctrl_wg.signal = 'pulse';
    ctrl_wg.sop = tb.sop;                       % Initial start of pulse index normalized to ctrl_wg.block_size = tb.subband_fft_size
    ctrl_wg.period = ctrl_wg.block_size;        % Pulse period
    if tb.chirp
        ctrl_wg.period = ctrl_wg.block_size+1;      % Pulse period for pulse moving in block like a sop chirp
    end
    if strcmp(tb.model_signal, 'impulse')
        ctrl_wg.width = 1;                      % Pulse width for FFT impulse response
    else  % default to 'square')
        ctrl_wg.ampl = 2*ctrl_wg.ampl;
        ctrl_wg.offset = -ctrl_wg.ampl/2;
        ctrl_wg.period = ctrl_wg.block_size/tb.subband_wg;  % Pulse period equal to subband_wg period
        ctrl_wg.width = ctrl_wg.period/2;       % Pulse width for square wave
    end
    ctrl_wg.width_remaining = 0;
end

%% Subband FFT parameters
ctrl_pfft_subband.fft_size = tb.subband_fft_size;
ctrl_pfft_subband.complex = false;  % real input PFB
ctrl_pfft_subband.gain = ctrl_pfft_subband.fft_size;
if strcmp(tb.model_quantization, 'floating point')
    ctrl_pfft_subband.data_w = 0;
else
    ctrl_pfft_subband.full_scale_w = 0;
    ctrl_pfft_subband.data_w = 16;
    ctrl_pfft_subband.data_q_full_scale = 2^(ctrl_pfft_subband.data_w-1);
end


%% Run the data path processing
t_start = cputime;
data_wg              = zeros(tb.nof_tsub, tb.subband_fft_size);
data_pfft_subband    = zeros(tb.nof_tsub, tb.nof_subbands);
for bi = 0:tb.nof_tsub-1
    bI = bi+1;
    
    % Data path (DP)
    [ctrl_wg, block_wg]           = wg(  ctrl_wg);
    [         block_pfft_subband] = pfft(ctrl_pfft_subband, block_wg);

    % Capture data at each DP interface
    data_wg(bI, :)           = block_wg;
    data_pfft_subband(bI, :) = block_pfft_subband;
end
t_stop = cputime;
disp(sprintf('Total processing time: %f seconds', t_stop-t_start));

%% Save data reference file for stimuli and verification of HDL implementation
if strcmp(tb.model_quantization, 'fixed point')
    % Quantize WG output
    wg_q_data = round(ctrl_wg.q_full_scale * data_wg);
    
    % Quantize PFFT subband output real and imaginary
    pfft_subband_q_data = round(ctrl_pfft_subband.data_q_full_scale * data_pfft_subband);

    % Save the data path signals
    tbegin = 1;
    tend = tb.nof_tsub;  % <= tb.nof_tsub    
    file_name_prefix = 'data/run_pfft_m_';    
    file_name = sprintf([file_name_prefix, '%s_%db_%dpoints_%db.dat'], ctrl_wg.name, ctrl_wg.data_w, tb.subband_fft_size, ctrl_pfft_subband.data_w);
    fid = fopen(file_name, 'w');
    fprintf(fid,'Nof lines WG output:   %d\n', length(wg_q_data(:)));
    fprintf(fid,'Nof lines PFFT output: %d\n', length(pfft_subband_q_data(:)));
    % WG output
    for bI = tbegin:tend
        for bJ = 1:tb.subband_fft_size
           fprintf(fid,'%d\n', wg_q_data(bI, bJ));
        end
    end;            
    % PFFT subband output real and imaginary
    for bI = tbegin:tend
        for bJ = 1:tb.nof_subbands
           fprintf(fid,'%10d%10d\n', real(pfft_subband_q_data(bI, bJ)), imag(pfft_subband_q_data(bI, bJ)));
        end
    end
    fclose(fid);
end


%% Plot data path results
xfig = 300;
yfig = 200;
xfigw = 1000;
yfigw = 800;
dfig = 20;

ts       = (0:tb.subband_fft_size*tb.nof_tsub-1)/tb.subband_fft_size;   % time of ADC / WG samples in subband periods
tsub_all = (0:    tb.nof_subbands*tb.nof_tsub-1)/tb.nof_subbands;       % time in subband periods for block of subbands
tsub_one = (0:                    tb.nof_tsub-1);                       % time in subband periods for one subband

sub_I      = tb.subband_I      + [0: tb.nof_subbands: tb.nof_subbands*tb.nof_tsub-1];  % get Matlab indices of all subband_I
sub_Iplus1 = tb.subband_Iplus1 + [0: tb.nof_subbands: tb.nof_subbands*tb.nof_tsub-1];  % get Matlab indices of all next neighbour subband_I+1

%% Plot WG output
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
data = data_wg.';
plot(ts, data(:))
ylim([-1.3 1.3]);
title(sprintf('WG output data (WG subband %6.3f)', tb.subband_wg));
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel('Voltage');
grid on;

%% Plot PFFT subbands real and imaginary for all tb.nof_tsub in one plot
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

data = data_pfft_subband.';
data = data(:);
hy = 1.2 * max(abs(data));
plot(tsub_all, real(data), 'r', tsub_all, imag(data), 'b');
ylim([-hy hy])
title(sprintf('Subband real and imag data (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
xlabel(sprintf('Subbands 0:%d at time 0:%d [Tsub]', tb.nof_subbands-1, tb.nof_tsub-1));
ylabel('Voltage');
grid on;

%% Plot PFFT subbands spectrum and phase for all tb.nof_tsub in one plot
sub_ampl = abs(data_pfft_subband);
sub_ampl_max = max(sub_ampl(:));
sub_phase = angle(data_pfft_subband)*180/pi;
x = sub_ampl < 0.1*sub_ampl_max;
sub_phase(x) = 0;   % force phase of too small signals to 0

fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
subplot(2,1,1);
data = sub_ampl.';
data = data(:);
plot(tsub_all, data, 'k', tsub_all(sub_I), data(sub_I), 'ko', tsub_all(sub_Iplus1), data(sub_Iplus1), 'kx');
title(sprintf('Subband data - amplitude  (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
xlabel(sprintf('Subbands 0:%d at time 0:%d [Tsub]', tb.nof_subbands-1, tb.nof_tsub-1));
ylabel('Voltage');
grid on;
subplot(2,1,2);
data = sub_phase.';
data = data(:);
plot(tsub_all, data, 'k', tsub_all(sub_I), data(sub_I), 'ko', tsub_all(sub_Iplus1), data(sub_Iplus1), 'kx');
ylim([-180 180])
title(sprintf('Subband data - phase  (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
xlabel(sprintf('Subbands 0:%d at time 0:%d [Tsub]', tb.nof_subbands-1, tb.nof_tsub-1));
ylabel('Phase [degrees]');
grid on;

%% Plot phase for the subband that is set in the WG
wg_sub_phase = angle(data_pfft_subband(:, tb.subband_I))*180/pi;
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);
plot(tsub_one, wg_sub_phase, '-o')
ylim([-180 180])
title(sprintf('Subband phase for subband %d in range 0:%d', tb.subband_i, tb.nof_subbands-1));
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel('Phase [degrees]');
grid on;

%% Plot PFFT subbands spectrum and phase for all tb.nof_tsub in separate plots
sub_ampl = abs(data_pfft_subband);
sub_ampl_max = max(sub_ampl(:));
sub_phase = angle(data_pfft_subband)*180/pi;
x = sub_ampl < 0.1*sub_ampl_max;
sub_phase(x) = 0;   % force phase of too small signals to 0

fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

if tb.plot_per_block
    for bi = 0:tb.nof_tsub-1
        bI = bi+1;
        subplot(2,1,1);
        data = sub_ampl(bI, :);
        plot(0:tb.nof_subbands-1, data, 'k', tb.subband_i, data(tb.subband_I), 'ko', tb.subband_iplus1, data(tb.subband_Iplus1), 'kx');
        ylim([0 sub_ampl_max])
        title(sprintf('Subband data - amplitude  (o,x = subband %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
        xlabel(sprintf('Subbands 0:%d at time %d [Tsub]', tb.nof_subbands-1, bi));
        ylabel('Voltage');
        grid on;
        subplot(2,1,2);
        data = sub_phase(bI, :);
        plot(0:tb.nof_subbands-1, data, 'k', tb.subband_i, data(tb.subband_I), 'ko', tb.subband_iplus1, data(tb.subband_Iplus1), 'kx');
        ylim([-180 180])
        title(sprintf('Subband data - amplitude  (o,x = subband index %d,%d for WG subband = %6.3f)', tb.subband_i, tb.subband_iplus1, tb.subband_wg));
        xlabel(sprintf('Subbands 0:%d at time %d [Tsub]', tb.nof_subbands-1, bi));
        ylabel('Phase [degrees]');
        grid on;
        drawnow;
        %pause(0.3)
    end
end
    
%% Plot subband spectrogram for all tb.nof_tsub in one plot
data = db(sub_ampl);                % no need to scale data, range is already normalized
if strcmp(tb.model_quantization, 'floating point')
    db_low = -150;
else
    db_low = -20 - floor(6.02 * ctrl_pfft_subband.data_w);
end
%db_low = floor(min(data(data~=-Inf)))
data(data<db_low) = db_low;
mymap = jet(-db_low);
%mymap(1,:) = [0 0 0];   % force black for dB(0)
colormap(mymap);
imagesc(data',[db_low 0]);
colorbar;
title(sprintf('Subband spectogram (max value = %f = %.2f dB)', sub_ampl_max, db(sub_ampl_max)));
xlabel(sprintf('Time 0:%d [Tsub]', tb.nof_tsub-1));
ylabel(sprintf('Subbands 0:%d', tb.nof_subbands-1));

