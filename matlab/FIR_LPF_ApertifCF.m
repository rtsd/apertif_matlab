%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: R. de Wild, 2015 (Original)
%         E. Kooistra, 2016
%
% Purpose : Calculate FIR filter coefficients for Apertif channel
%           filterbank using fircls1.
% Description :

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% workspace initiation
clear all
close all
fig=0;

% preset FIR-filter parameters
K        = 64;          % Number of channels
Taps     = 8;           % Number of taps, even 4 could be sufficient (erko)
M        = Taps*K;      % Number of filter coefficients (taps)
h_ix     = 1:M;         % coefficient index
nbit     = 9;           % bits/coefficient = 1 sign + (nbit-1) mantissa
fs       = 781250;      % sampling clock = 800.0 MHz / 1024       
f_nyq    = fs/2;        % Nyquist frequency
f_pb     = fs/K/2;      % end of passband 
f_chan   = fs/K;
                   
% normalized frequency axis & normalized amplitude axis
w_pb     = f_pb/f_nyq;  % = f_chan

% The fircls1 pass band deviation and stop band deviation are positive
% fractions of 1. The ripple in dB needs to be divided by 10 to get from
% dB = 10*log10() the log10() and divided by 2 because it is a relative
% voltage value (instead of a relative power value).
% When the pass band ripple in dB is positive then the linear factor is
% sligthly larger than 1, so subtract 1 to get the pass band deviation.
% The pass band deviation defines the +- fraction around 1. The stop band
% 'ripple' in dB is negative and defines the attenuation. The stop band
% deviation is the fraction of 1 that remains in the stop band. Therefore
% both the pass band ripple and the stop band attenuation result both in
% linear deviations that are close to 0.
% The fircls1 description defines DEVP is the maximum passband deviation
% or ripple (in linear units) from 1, and DEVS is the maximum stopband
% deviation or ripple (in linear units) from 0.
r_pb     = 10^(0.025)-1;  % passband ripple = 0.5 dB
r_sb     = 10^(-3.0);     % stopband ripple = -60.0 dB

% compute M filter-coefficients
h        = fircls1(M-1, w_pb, r_pb, r_sb); 
hfs_abs  = abs(fftshift(fft(h ,M))); 

% compute quantized filter coefficients
hmax      = max(h);
hq        = double( uencode(h, nbit, hmax, 'signed') );
hqfs_abs  = abs(fftshift (fft( hq ,M))) * sum(h)/sum(hq);  % normalize to response for DC

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print FIR-filter parameters 
sprintf([ ...
    ' fs     = sample rate            = %6.2f [Hz] \n' , ...
    ' f_chan = channel bandwidth      = %6.2f [Hz]\n' , ...
    ' r_pb   = pass band deviation    = %9.7f = %6.2f dB_W ripple\n' , ...
    ' r_sb   = stop band deviation    = %9.7f = %6.2f dB_W attenuation\n' , ...
    ' M      = number of coefficients = %d'], ...
    fs, f_chan, r_pb, 20*log10(1+r_pb), r_sb, 20*log10(r_sb), M)

% Save FIR-filter-coefficients 
% N.B. - read '.txt'-file with 'wordpad' or use \r\n !!!
file_name = ['FIR_LPF_ApertifCF.txt'];
FIRtable  = [ h(h_ix) ] ;
fid       = fopen(file_name , 'w');
fprintf(fid ,'%14.10f \n', FIRtable);
fclose(fid);

% Plot FIR-filter coefficients
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(hq);
title(['FIR filter coefficients (', num2str(nbit), ' bits)'] );
grid on;

% Plot FIR-filter amplitude characteristic
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
f = (h_ix - M/2-1) * fs/M / f_chan;
plot ( f, 20.0 * log10 ( hfs_abs ), 'r-', ...
       f, 20.0 * log10 ( hqfs_abs ), 'k-') ; 
xlo = f(1);
xhi = f(M);
xlo = -3;
xhi = 3;
xlim( [xlo xhi] );
grid on; 
zoom on; 
title('FIR filter transfer function');
text( xlo,  -5, ['sampling rate = ', num2str(fs), ' [Hz]']);
text( xlo, -10, ['channel bandwidth = ', num2str(f_chan), ' [Hz]']);
text( xlo, -15, ['number of taps = ', num2str(Taps)]);
text( xlo, -20, ['number of channels = ', num2str(K)]);
text( xlo, -25, ['number of coefficients = ', num2str(M)]);
text( xlo, -30, ['number of bits/coefficient = ', num2str(nbit)]);
xlabel('Frequency [channels]');
ylabel('Power [dB]');
print -dpng 'arts_fir';
