%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : FFT per block of data
% Description :
%
%   M = ctrl.fft_size
%
% in_data(1, M) = 
%      1                    M-1          M
%   1 |in_data(n-(M-1)) ... in_data(n-1) in_data(n-0)|
%
% If ctrl.complex then:
%    out_data(1, M) = fftshift(fft(in_data, M))
% else for real input data:
%    out_data(1, M/2) = first half of fft(in_data, M)
%
% If ctrl.gain=M then the out_data has the same range as the in_data.
%
% When ctrl.data_w = 0 then the processing uses floating point else the
% out_data is quantized to ctrl.data_w bits.
%
% The quantized out_data is normalized to full_scale = 2^ctrl.full_scale_w
% before quantization. 

function [out_data] = pfft(ctrl, in_data)

% Data processing
out_data = fft(in_data, ctrl.fft_size);

if ctrl.complex
    % shift negative frequencies half to the left to have zero-frequency component at center of spectrum
    out_data = fftshift(out_data);
else
    % because we have real input, we can omit the negative frequencies half of the fft result
    out_data = out_data(1:ctrl.fft_size/2);
end

% Normalize output to [-1 1]
out_data = out_data / ctrl.gain;

% Quantization
if ctrl.data_w>0
    full_scale = 2^ctrl.full_scale_w;
    out_data = quantize(out_data, full_scale, ctrl.data_w, 'half_away');
end
