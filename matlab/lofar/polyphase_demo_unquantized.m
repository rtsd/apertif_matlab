% Create a DFT Polyphase FIR Quantized Filter Bank.
% Initialize two variables to define the filters and the filter 
% bank.
% M = 16;   % Number of channels in the filter bank.
% N = 8;    % Number of taps in each FIR filter.
M = 256;    % Number of channels in the filter bank.
N = 16;     % Number of taps in each FIR filter.
L = M*N-2;  % Prototype FIR filter order (L+1 taps)


% Compute the prototype FIR low-pass filter (order L, length L+1)

%% Original filter design from MathWorks example from:
%% http://www.mathworks.nl/access/helpdesk_r12p1/help/toolbox/filterdesign/quant10c.shtml
%% Calculate the coefficients b for the prototype lowpass filter, 
%% and zero-pad so that it has length M*N.
% b = fir1(M*N-2,1/M);
% b = [b,zeros(1,M*N-length(b))];

% Equiripple filter design, for 1st filterbank in LOFAR station
Fs = 64000;  % Sampling Frequency in kHz
Fpass = 125;   % Passband Frequency
Fstop = 170;   % Stopband Frequency; high transition band (40kHz) required 
% to meet ripple requirements
Wpass = 1;     % Passband Weight (increase Wpass to reduce passband ripple)
Wstop = 200;   % Stopband Weight (increase Wstop to reduce stopband ripple)
dens  = 16;    % Density Factor


% Calculate the coefficients using the REMEZ function.
disp('Computing the prototype FIR low-pass filter');
b  = remez(L, [0 Fpass Fstop Fs/2]/(Fs/2), [1 1 0 0], [Wpass Wstop], {dens});
% Correction of strange peaks at tails
b(1)=b(2)/2;
b(end)=b(end-1)/2;
% zero-pad so that it has length M*N.
b = [b, zeros(1,M*N-length(b))];

% b=protofir2;

% Reshape the filter coefficients into a matrix whos rows 
% represent the individual polyphase FIR filters' responses
B = flipud(reshape(b,M,N));
Hd = cell(M,1);
for k=1:M
  Hd{k} = dfilt.dffir(B(k,:));
end

% Create a quantized FFT F of length M.
% Construct a bank of M  filters and an M-point FFT. 
% Filter a sinusoid that is stepped in frequency from 0 to SWEEPRANGE
% (<=pi) radians, store the power of the filtered signal, and plot the 
% results for each channel in the filter bank.
sweeprange=pi;
sweepbands=M/2;
if M>=64
    sweeprange=16*pi/M;
    sweepbands=8;
end

Nfreq = 200; % Number of frequencies to sweep.
w = linspace(0,sweeprange,Nfreq);  % Frequency vector from 0 to SWEEPRANGE.
P = 100;     % Number of output points from each channel.
t = 1:M*N*P; % Time vector.

Ampl=sqrt(2); % amplitude of sine-wave with power = 1
HH  = zeros(M,length(w));  % Stores output power for each channel.
disp('Frequency sweep');
for j=1:length(w)
  % display progress  
  fprintf('.');
  if mod(j,50)==0
      fprintf('\n');
  elseif mod(j,10)==0 
      fprintf(' ');
  end

  % generate input signal
  x = Ampl*sin(w(j)*t);

  % EXECUTE THE FILTER BANK:
  % Reshape the input so that it represents parallel channels of
  % data going into the filter bank.
  X = [x(:);zeros(M*ceil(length(x)/M)-length(x), 1)];
  X = reshape(X,M,length(X)/M);
  % Make the output the same size as the input.
  Y  = zeros(size(X));   
  Yf = zeros(size(X));   
  
  % Perform the FIR filtering for each band filter
  for k=1:M
    Y(k,:) = filter(Hd{k},X(k,:));  
  end  
  % perform the FFT
  Yf = fft(Y,M);
  % Store the output power
  HH (:,j) = signal_power(Yf.')';       
  
end

% compensate for -3dB pass-band transfer
% (This is a result of the real signals in bands 2 to M/2 being 
% split between 2 bands, band k and band M-k, k=1...M/2
for k=2:M/2
    HH(k,:)=2*HH(k,:);
end
for k=M/2+2:M
    HH(k,:)=2*HH(k,:);
end

% Analytical computation of the shifted versions hh of the prototype b
disp('computing the shifted versions of the prototype FIR');
% first, compute the complex exponential shift factor
WM=complex(cos(2*pi/M),-sin(2*pi/M));
% now perform the shift operations
hh=zeros(M,L);
for k=0:M-1
  for n=0:L-1
    hh(k+1,n+1)=b(n+1)*WM^(k*n);
  end
end
% hh(m,:) is the impulse response of the m-th band filter
% The values are complex, and the response length is L=M*N

% now compute the frequency response of all of these band filters
Hb=20*log10(abs(fft(hh',M*N)));
Hb=Hb';
% Hb(m,:) is the frequency amplitude response (in dB) of the m-th
% band-filter.

% Compute the frequency amplitude response (in dB) of the prototype
% low-pass FIR filter
Hproto=20*log10(abs(fft(b,M*N)));
% compute the corresponding frequency values (for plotting)
fH=[0:Fs/(M*N):Fs*(1-1/(M*N))];

% Let the user know simulations are ready
beep;pause(0.1);beep;pause(0.1);beep;pause(0.1);beep;

% Plot the results.
figure;
% compute the sweep frequency values from w
f=w*Fs/(2*pi);
% compute the max. frequency of the sweep range
sweeprange_f=sweeprange*Fs/(2*pi);
% plot prototype FIR filter response
plot(fH,Hproto,'r:');
hold on;
% initiate legends
Legends='prototype';
% define colors (used in a rotating sequence)
colors='bgkcrm';
% determine number of bands to be plotted
plotbands=sweepbands;
if plotbands<M/2+1
    plotbands=plotbands+1;
end
% plot the sweep responses of the filter bank for the first PLOTBANDS band
% filters
for c=1:plotbands
  plot(f,10*log10(HH(c,:)),colors(1+mod(c,6)));
  Legends=str2mat(Legends,sprintf('band %2d',c-1));
end
title(sprintf('Polyphase/DFT Filter Bank Frequency Response, sine-wave sweep, first %d bands',plotbands));
xlabel('Frequency [kHz]');
ylabel('Magnitude Response (dB)');
axis([0 sweeprange_f -100 5]);
legend(Legends,4);
grid on;

% Now plot the band filter responses constructed by shifting in a new
% figure
figure;
hold off;
for c=1:plotbands
    plot(fH,Hb(c,:),colors(1+mod(c,6)));  
    hold on;
    if c==1 Legends='band  0'; else Legends=str2mat(Legends,sprintf('band %2d',c-1)); end;
end
title(sprintf('Filter Bank Frequency Response, FFT-computed, first %d bands',plotbands));
xlabel('Frequency [kHz]');
ylabel('Magnitude Response (dB)');
axis([0 sweeprange_f -100 5]);
legend(Legends,4);
grid on;

return
