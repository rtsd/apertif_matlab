%-----------------------------------------------------------------------------
%
% Copyright (C) 2019
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%--------------------------------------------------------------------------
% Author: E. Kooistra, 2019
%
% Purpose : Clip input s into range [clip_min, clip_max]
function [s_clip] = clip(s, clip_min, clip_max)
    s_clip = s;
    if s > clip_max
        s_clip = clip_max;
    elseif s < clip_min
        s_clip = clip_min;
    end
end

