% Removal of the subband filter response caused by the Lofar backend.
% Erik van der Wal, october 2010
% April, 2015: changed the example from Embrace to Apertif/Uniboard numbers.
% Feb, 2016: improved performance near subband edge

clear all;
close all;

% some constants
fsample = 800e6;
bw = 20e3; % resolution within a subband, in Hz

% Determine number of FFT bins
fftbins = 2^ceil(log(fsample/1024 / bw)/log(2))
bw_realized = fsample/1024 / fftbins

% Load filter coefficients and pad with zeros
flt = load('../data/Coeffs16384Kaiser-quant.dat')';
flt(1024*fftbins) = 0;
% generate frequency response
flt = abs(fft(flt)).^2;
flt = flt * fftbins / sum(flt);

% generate a fake spectrum
t = (0:(1024/2 * fftbins)) / ((1024/2 * fftbins)) * 2 * pi;
s = 2 + (sin((t-1)*100)./((t-1)*100)).^5;
s = s + 2*(sin((t-1.03)*200)./((t-1.03)*200)).^5;
s = s + (sin((t-1.01)*400)./((t-1.01)*400)).^5;
s = s + randn(size(s)) / 10; % add some random noise
s = s + (1:numel(s))/1e5;
% generate the negative frequencies
s = [s, fliplr(s(2:end-1))];

% generate proper aliased results
pm = zeros(1, 1024 * fftbins);
for subband=1:1024
	pm((subband-1)*fftbins+(1:fftbins)) = circshift(sum(reshape(s .* circshift(flt, [0,fftbins * (subband - 1)]), fftbins, []),2), [fftbins/2,0]);
end
pm = circshift(pm,[0,-fftbins/2]);

% only obtain center 384 subbands
pm = pm((63.5*fftbins + 1):((63.5+384) * fftbins)); % pm = power measured
s = s((63.5*fftbins + 1):((63.5+384) * fftbins));

% Only use the subband with the filter passband and its neighbouring subbands
subf = circshift(flt, [0,fftbins * 1.5]);
% Reorganize data for convenient access when deconvolving
subf = reshape(subf(1:(3 * fftbins)), fftbins, []);
pmr = reshape(pm, fftbins, []); % pmr = power measured reshaped
s_fit = NaN(size(pmr));

% estimate (de)convolution length
err = 0.001;
declen = ceil(log(err)/log(subf(2,3)/subf(2,2)))
%declen = 384; % length of (de)convolution
%declen=2;

% start with bin at center of subband, assume no aliasing
bin = 1+(fftbins/2);
s_fit(bin,:) = pmr(bin,:) / subf(bin,2);

% % matrix implementation:
% for bin = (fftbins/2):-1:2
%   f = diag(subf(bin,2) * ones(385,1));
%   f += shift(diag(subf(bin,3) * ones(385,1)),1);
%   f(1,end) = 0;
%   invf = inv(f);
%   invf0 = invf(2:end,1);
%   invfrest = invf(2:end,2:end);
%   s_fit(bin,:) = invfrest * pmr(bin,:)';
%   s_hat = s_fit(bin+1,:);
%   d_missing = invf0 \ (s_hat - s_fit(bin,:))';
%   s_fit(bin,:) += invf0' * d_missing;
% end

% convolution implementation:
% work our way from the middle down the bins, ignoring subf(3)
for bin = (fftbins/2):-1:2
	dec = (-subf(bin,3)/subf(bin,2)).^(declen:-1:1) / -subf(bin,3);
  tmp = conv(pmr(bin,:), dec);
  s_fit(bin,:) = tmp(end-383:end);
  % compensate for missing sample
  if bin <= declen
    dec = dec * -subf(bin,3)/subf(bin,2);
    s_hat = s_fit(bin+1,:); % assume similar to prev. bin
    d_missing = dec' \ (s_hat(end-(declen:-1:1)+1) - s_fit(bin,end-(declen:-1:1)+1))';
    s_fit(bin,end-(declen:-1:1)+1) = s_fit(bin,end-(declen:-1:1)+1) + d_missing * dec;
  end
end

% also work our way from the middle up the bins, ignoring subf(1)
for bin = (fftbins/2 + 2):fftbins
  dec = (-subf(bin,1)/subf(bin,2)).^(1:declen) / -subf(bin,1);
  tmp = conv(pmr(bin,:), dec);
  s_fit(bin,:) = tmp(1:384);
  % compensate for missing sample
  if bin > fftbins - declen
    dec = dec * -subf(bin,1)/subf(bin,2);
    s_hat = s_fit(bin - 1,:); % assume similar to neigbouring bin
    d_missing = dec' \ (s_hat(1:declen)' - s_fit(bin,1:declen)');
    s_fit(bin,1:declen) = s_fit(bin,1:declen) + d_missing * dec;
  end
end
% finally solve the first fft bin of each subband
bin = 1;
dec = (-subf(bin,3)/subf(bin,2)).^(383:-1:0) / subf(bin,2);
tmp = conv(pmr(1,:), dec);
s_fit(bin,:) = tmp(end-383:end);
% compensate for missing sample
dec = dec * -subf(bin,3)/subf(bin,2);
s_hat = (s_fit(2,:) + circshift(s_fit(end,:),[0,1])) / 2;
s_hat(1) = s_fit(2,1);
d_missing = dec(4:end-4)' \ (s_hat(4:end-4)' - s_fit(bin,4:end-4)');
s_fit(bin,:) = s_fit(bin,:) + d_missing * dec;

% undo reshape
s_fit = real(s_fit(:));

% Finally, plot results.

x = (1:(384*fftbins))/fftbins+63.5;
xl = [78, 88];
yl = [1, 5];

subplot(2,2,1);
plot(x,s);
xlim(xl);
ylim(yl);
title('Input data');
xlabel('subband');
ylabel('magnitude');

subplot(2,2,2);
plot(x,pm);
xlim(xl);
ylim(yl);
title('Including aliased filter response');
xlabel('subband');
ylabel('magnitude');

subplot(2,2,3);
plot(x,s_fit);
xlim(xl);
ylim(yl);
title('Deconvolved data');
xlabel('subband');
ylabel('magnitude');

subplot(2,2,4);
plot(x, s_fit(:).' - s);
xlim(xl);
%ylim([-0.01, 0.01]);
title('Error');
xlabel('subband');
ylabel('magnitude');

print('unripple_v2.jpg', '-djpeg')