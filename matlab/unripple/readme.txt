Hoi Eric,

Zie de bijlage en de mail hieronder. 
unripple_v2.m is een octave script van Erik. 
unripple_v2.pdf is een korte toelichting van Erik. 
unripple_v2_matlab.m is een gewijzigde versie van unripple_v2.m, heb ik aangepast om met Matlab uit te voeren. 
Coeffs16384Kaiser-quant.dat zijn de gebruikte filtercoefficienten. 
cc.pdf is achtergrondinformatie van Erik (of misschien zelfs nog van Dion). 

Vriendelijke groet, 
Boudewijn

>>> Erik van der Wal 02/29/16 4:59 PM >>>

Boudwijn,
 
Nog even nagedacht, en gerekend. Voor cross-correlatie is alles exact gelijk. Het enige is dat waar ik de hermitian transpose (') gebruik, deze moet worden vervangen door een echte transpose (.').
Voor zover ik kan kijken zijn dit alleen de regels die beginnen met "d_missing =".
Maar omdat ik dit overal op die regel al gebruik, gaat het zelfs daar waarschijnlijk al goed zonder aanpassing.
 
Erik.

>>> Erik van der Wal 2/28/2016 6:59 PM >>>
Boudewijn,

Ik kon het niet op me laten zitten om toch het probleem op de rand van een subband op te lossen.
Even zitten prutsen, maar het lijkt een redelijk stabiele oplossing op te leveren.
Uiteindelijk wordt de 'buurman' van de FFT bin gebruikt om af te schatten wat de waarde moet zijn.
Op dit moment gebruik ik domweg een least squares algorithme, zonder flagging.
In de bijgevoegde beschrijving wordt het geheel uitgelegd met inverse matrices.
Het geheel kan efficient worden geprogrammeerd door een 3 punts convolutie te gebruiken om de inverse te berekenen.
In octave is dit een factor 10 sneller, in Matlab waarschijnlijk nog meer verschil.
Verder zat ik nog te prutsen met een halve subband in de vorige versie, die is er nu uitgehaald.
Kijk maar eens.

Erik.

