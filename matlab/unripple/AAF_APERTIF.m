%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author : Erik van der Wal
%          SJW, 11 October 2016
%
%% Anti-aliasing filter (AAF) for APERTIF
%
% APERTIF uses a critically sampled filterbank to perform spectral
% separation of the baseband signals into subbands. A disadvantage of such
% an approach is that the signals at the band edges are affected by
% aliasing. In this script, we demonstrate the possibility of removing
% (mosst of) these aliasing effects by using an anti-aliasing filter (AAF).
% The AAF is based on an approach developed by Erik van der Wal in the
% context of EMBRACE.
%
% To validate the AAF, this script will be set up in such a way that
% multiple input signals can be generated with different signal delays such
% that the performance of the AAF can be studied in the presence of
% standard operations like beamforming and correlation. However, no such
% features are added yet, i.e., the anti-aliasing filter is only
% demonstrated on a single input signal.
%

%% start with a clean workspace
clear
close all

%% parameters
fstart = 1.2e9;                 % start frequency of analog passband in Hz
fstop = 1.6e9;                  % stop frequency of analog passband in Hz
fADC = 2 * (fstop - fstart);    % assume Nyquist sampling at ADC
Nsb = 1024;                     % number of subbands
BWsb = fADC / 1024;             % subband bandwidth in Hz
Nch = 64;                       % number of channels per subband
BWch = BWsb / 64;               % channel bandwidth in Hz
fsignal = 1.42e9;               % reference frequency for signal
Nsbp = 384;                     % number of processed subbands
sbstart = 64;                   % first of Nsbp consecutive subbands
tol = 1e-3;                     % ignore filter response below this level

%% load filter coefficients and pad with zeros
FIRcoeff = load('../data/Coeffs16384Kaiser-quant.dat')';
% filter has length 16384, zero-padded to match total number of channels
FIRcoeff(16385:Nsb * Nch) = 0;
% generate frequency response
FIRspec = abs(fft(FIRcoeff)).^2;
FIRspec = FIRspec * Nch / sum(FIRspec);

%% generate fake spectrum
freq = fstart:BWch:fstop;
omega = 2 * pi * (0:(Nch * Nsb / 2)) / (Nch * Nsb / 2);
omega0 = 2 * pi * (fsignal - fstart) / (fstop - fstart);
s = 2 + (sin((omega - omega0) * 100) ./ ((omega - omega0) * 100)).^5;
s = s + 2 * (sin((omega - 1.015 * omega0) * 200) ./ ((omega - 1.015 * omega0) * 200)).^5;
s = s + (sin((omega - 1.005 * omega0) * 400) ./ ((omega - 1.005 * omega0) * 400)).^5;
% add random noise
s = s + randn(size(s)) / 10;
% add slope
s = s + (1:numel(s))/1e5;
% generate the negative frequencies
s = [s, fliplr(s(2:end-1))];

%% generated properly aliased results
smeas = zeros(1, Nch * Nsb);
for sbidx = 1:Nsb
    smeas((sbidx - 1) * Nch + (1:Nch)) = circshift(sum(reshape(s .* circshift(FIRspec, [0, Nch * (sbidx - 1)]), Nch, []), 2), [Nch/2, 0]);
end
smeas = circshift(smeas, [0, -Nch/2]);

%% subband selection
chsel = ((sbstart - 0.5) * Nch + 1):((sbstart + Nsbp - 0.5) * Nch);
s = s(chsel);
smeas = smeas(chsel);

%% reorganize data for convenient access during deconvolution
% we will only use the passband of the current subband and its neighbours
sbfilter = circshift(FIRspec, [0, Nch * 1.5]);
sbfilter = reshape(sbfilter(1:3 * Nch), Nch, []);
% reshape the measured powers for convenience
smeasr = reshape(smeas, Nch, []);
% allocate memore for result after application of AAF
scorr = NaN(size(smeasr));

%% estimate (de)convolution length
declen = ceil(log(tol)/log(sbfilter(2,3)/sbfilter(2,2)));

%% apply AAF (convolution implementation)
% assume that there is no aliasing in center of the subband
chidx = 1 + Nch/2;
scorr(chidx, :) = smeasr(chidx, :) ./ sbfilter(chidx, 2);

% work from central channel downwards, ignore response of previous subband
for chidx = Nch/2:-1:2
    dec = (-sbfilter(chidx,3)/sbfilter(chidx,2)).^(declen:-1:1) / -sbfilter(chidx, 3);
    tmp = conv(smeasr(chidx, :), dec);
    scorr(chidx, :) = tmp(end - (Nsbp - 1):end);
    % compensate for missing sample
    if chidx <= declen
        dec = dec * -sbfilter(chidx, 3) / sbfilter(chidx, 2);
        % estimate of missing data, use neighbouring channel as initial estimate
        shat = scorr(chidx+1,:);
        dmissing = dec' \ (shat(end - (declen:-1:1) + 1) - scorr(chidx, end - (declen:-1:1) + 1))';
        scorr(chidx,end - (declen:-1:1) + 1) = scorr(chidx, end - (declen:-1:1) + 1) + dmissing * dec;
    end
end

% work from central channel upward, ignore response of next subband
for chidx = 2 + Nch/2:Nch
    dec = (-sbfilter(chidx,1)/sbfilter(chidx,2)).^(1:declen) / -sbfilter(chidx, 1);
    tmp = conv(smeasr(chidx, :), dec);
    scorr(chidx, :) = tmp(1:Nsbp);
    % compensate for missing sample
    if chidx > Nch - declen
        dec = dec * -sbfilter(chidx, 1) / sbfilter(chidx, 2);
        % estimate of missing data, use neighbouring channel as initial estimate
        shat = scorr(chidx-1,:);
        dmissing = dec' \ (shat(1:declen) - scorr(chidx, 1:declen))';
        scorr(chidx,1:declen) = scorr(chidx, 1:declen) + dmissing * dec;
    end
end

% finally solve the first fft bin of each subband
chidx = 1;
edgelen = 4;    % number of subbands to ignore at edge of passband
dec = (-sbfilter(chidx,3)/sbfilter(chidx,2)).^((Nsbp-1):-1:0) / sbfilter(chidx,2);
tmp = conv(smeasr(chidx,:), dec);
scorr(chidx, :) = tmp(end - (Nsbp-1):end);
% compensate for missing sample
dec = dec * -sbfilter(chidx,3)/sbfilter(chidx,2);
% estimate of missing data, use average of first and last channel as
% initial estimate
shat = (scorr(chidx+1,:) + circshift(scorr(end, :), [0, 1])) / 2;
shat(1) = scorr(chidx+1, 1);
dmissing = dec(edgelen:end-edgelen)' \ (shat(edgelen:end-edgelen) - scorr(chidx,edgelen:end-edgelen))';
scorr(chidx, :) = scorr(chidx, :) + dmissing * dec;

% undo reshape
scorr = scorr(:).';

%% show result
figure
subplot(2,2,1);
plot(freq(chsel),s);
title('Input data');
xlabel('frequency (Hz)');
ylabel('magnitude');
% zoom in on 16 subband wide interval around feature
axis([fsignal - 8 * BWsb, fsignal + 8 * BWsb, 1 4.5])

subplot(2,2,2);
plot(freq(chsel),smeas);
title('Including aliased filter response');
xlabel('frequency (Hz)');
ylabel('magnitude');
% zoom in on 16 subband wide interval around feature
axis([fsignal - 8 * BWsb, fsignal + 8 * BWsb, 1 4.5])

subplot(2,2,3);
plot(freq(chsel),scorr);
title('Deconvolved data');
xlabel('frequency (Hz)');
ylabel('magnitude');
% zoom in on 16 subband wide interval around feature
axis([fsignal - 8 * BWsb, fsignal + 8 * BWsb, 1 4.5])

subplot(2,2,4);
plot(freq(chsel), scorr - s);
title('Error');
xlabel('frequency (Hz)');
ylabel('magnitude');
% zoom in on 16 subband wide interval around feature
axis([fsignal - 8 * BWsb, fsignal + 8 * BWsb, -0.02 0.02])
