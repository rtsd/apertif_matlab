%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%--------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : Polyphase FIR filter (PFIR) per block of data
% Description :
%
%    M = ctrl.nof_polyphases = FFT size
%    R = ctrl.nof_taps = nof taps per polyphase path
%    N = RM = total nof taps = total FIR filter length
%
% ctrl.coeff(M, R) =
%      1                2                     R
%   1 |h(M-1)           h(2M-1)           ... h(RM-1) = H(N-1)  |
%     |...                 ...                                  |
%     |h(1)             h(M+1)                                  |
%   M |h(0)             h(M)              ... h((R-1)*M)        |
%
% z(M, R) = [in_data', ctrl.Zdelays] =
%      1                2                     R
%   1 |in_data(n-(M-1)) in_data(n-(2M-1)) ... in_data(n-(RM-1)) |
%     |...                                    ...               |
%     |in_data(n-1)     in_data(n-(M+1))                        |
%   M |in_data(n-0)     in_data(n-M)      ... in_data(n-(R-1)*M)|
%
% out_data(1, M)' = sum(ctrl.coeff .* z, 2) =
%   1 |out_data(n-(M-1))|
%     |...              |
%     |out_data(n-1)    |
%   M |out_data(n-0)    |
%
% Internally there are two implementation variants that are equivalent:
% . When c_use_matlab_filter=0 then the matrix multipy of coeff and z and
%   the sum() per polyphase path are used.
% . When c_use_matlab_filter=1 then instead the Matlab filter() function
%   with internal Zdelays is used per polyphase path.
%
% Typically the ctrl.coeff(:) are scaled to fit in range +-1. The impulse
% response, and thus the coefficients, of the PFIR has a sync pulse shape.
% Therefore the DC response is mostly dependent on the value of the center
% tap coefficient, so with max(ctrl.coeff(:)) = 1 the DC gain of the PFIR
% will also be close to 1. The actual DC gain of the PFIR is
% sum(ctrl.coeff(:) / M (is close to 1).
%
% The PFIR typically has no other internal scaling, so ctrl.gain = 1.
% With DC gain close to 1 the out_data has the same scale as the in_data,
% so +-1. However the range of out_data can overshoot the normalized range
% of in_data [-1,1], e.g. when the input changes abruptly.
%
% When ctrl.data_w = 0 then the processing uses floating point else when
% ctrl.data_w > 0 then the out_data is quantized to ctrl.data_w bits.
%
% The quantized out_data is normalized to full_scale = 2^ctrl.overshoot_w
% before quantization to ensure that it can fit the PFIR overshoot. 
function [state, out_data] = pfir(ctrl, in_data)

if ctrl.bypass
    out_data = in_data;
else
    % Data processing
    c_use_matlab_filter = 0;
    if c_use_matlab_filter
        for I = 1:ctrl.nof_polyphases
            [out_data(I), ctrl.Zdelays(I,:)] = filter(ctrl.coeff(I,:), 1, in_data(I), ctrl.Zdelays(I,:));
        end
    else
        z = [in_data', ctrl.Zdelays];
        y = ctrl.coeff .* z;
        s = sum(y, 2);           % sum each polyphase path
        out_data = s';
        
        % shift in_data into Zdelays to prepare for next block of in_data
        ctrl.Zdelays = z(:,1:ctrl.nof_taps-1);
    end
    
    % Normalize output to [-1 1]
    out_data = out_data / ctrl.gain;
    
    % Quantization
    if ctrl.data_w>0
        full_scale = 2^ctrl.overshoot_w;
        out_data = quantize(out_data, full_scale, ctrl.data_w, 'half_away');
    end
end

% Keep state for next call
state = ctrl;
