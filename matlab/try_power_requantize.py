###############################################################################
#
# Copyright (C) 2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Author: Eric Kooistra, 14 Sep 2018,

"""Investigate requantization of data and power data for e.g. Stokes I and
   IQUV in Arts

1) General:

Assume only the system noise is relevant for the data_w, because channels with
RFI are excluded anyway. With a sigma of e.g. 8 units, so 3 bit the maximum
3 sigma noise then adds about log2(3) ~= 2 bits and 1 sign bit, so data_w is
then 6 bit to fit the Gaussian noise.

For the power the data is squared and the power of the noise becomes sigma**2.
To represent this power without introducing too much quantization noise at
most about sigma_w LSbits can be removed from the power data using truncation
or rounding. Truncation seems fine, because the auto power is > 0, so there
is no truncation asymmetry for < 0. Minimizing the amount of power bits is a
balance between avoiding too much quantization noise and avoiding too much
clipping. If power_w < 2*data_w - power_lsb_w - sign_w there will be clipping.
Cross power data is signed data, so then symmetric rounding is needed to avoid
introducing DC. Therefore default better use symmetric rounding rather than
asymmetric truncation.


2) Regarding Stoke I,Q,U,V power data:

For auto power Stokes I = XX* + YY* the power value is unsigned, so this
makes that the sign bit can be skipped and used as an extra power data bit to
fit the summation, so I < 2XX* and I < 2YY*.
For Stokes Q = XX* - YY* the power value is signed due to the subtraction, but
the subtraction also makes that that abs(Q) < largest(XX*, YY*).
The polarization cross powers U = 2*Re{XY*} and V = -2*Im{XY*} are signed too.
Compared to I,Q it may be benificial to transport the U,V without applying
the factor 2 and then scale them by 2 before I,Q,U,V are processed further.

Usage:
> python try_power_requantize.py

"""

import math
import common as cm
import test_plot as tp

# Model data with noise that has a sigma of sigma_w bits
sigma_w = 3
sigma = 2**sigma_w - 1        # unsigned sigma
noise_max = 4*sigma           # so 3 * sigma signed noise fits in sigma_w + log2(3) + 1 ~= 6b
noise_w = cm.ceil_log2(noise_max)
sign_w = 1                           # sign bit
data_w = noise_w + sign_w     # signed data needed to represent the noise
print 'noise sigma =', sigma
print 'noise max   =', noise_max

# For the power of the data the noise power will have 2*data_w bits with 2*sigma_w for the noise, so
# typically power_lsb_w = sigma_w bits could be removed.
power_lsb_w = sigma_w
# For power_w < 2*data_w - power_lsb_w - sign_w there will be clipping. It is fine to skip double
# sign bit because most negative data value -2**(data_w-1) does not occur in data
power_w = 2*data_w - power_lsb_w - sign_w

# Optionally overrule noise based data_w and power_w
#data_w = 8
#power_w = data_w

print 'data_w      =', data_w, 'bits'
print 'power_w     =', power_w, 'bits'
print 'power_lsb_w =', power_lsb_w, 'bits'

data = range(-noise_max,noise_max+1)   # system noise input data
data_abs = cm.abs_list(data)           # reference data for sqrt(power)
power_trunc = [cm.int_clip( cm.int_truncate((d**2), power_lsb_w), power_w)<<power_lsb_w for d in data]
power_round = [cm.int_clip( cm.int_round(   (d**2), power_lsb_w), power_w)<<power_lsb_w for d in data]
data_trunc = [int(round(math.sqrt(p))) for p in power_trunc]
data_round = [int(round(math.sqrt(p))) for p in power_trunc]

#print 'power_trunc =', power_trunc
#print 'power_round =', power_round

print 'data_abs    =', data_abs  
print 'data_trunc  =', data_trunc
print 'data_round  =', data_round
