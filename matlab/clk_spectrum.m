%-----------------------------------------------------------------------------
%
% Copyright (C) 2019
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2019
%
% Purpose : Spectrum of random toggling at certain clock frequency

clear all;
close all;
fig=0;

%% Settings
mode = 'square';
%mode = 'random';
mode = 'combine';
if strcmp(mode, 'square')
    M = 1;                    % Number of spectra to sum
else
    M = 1000;                 % Number of spectra to sum
end
N = 1024;                   % FFT size
fs = 800;                   % sample frequency
fclk = 25;                  % clock frequency
%fclk = 125;                  % clock frequency
t =(0:N-1)/fs;              % time axis
f = (0:N/2-1)/N * fs;       % positive frequency axis

%% Random logic signal toggling at clock frequency fclk
clk = sin(2*pi*fclk*t);
clk = (clk>0) * 2 - 1;                % clock square wave
prev_clk = clk(N);
sq = zeros(size(clk));
data = zeros(size(clk));
YYsum = zeros(1, N/2);

for m = 1:M
    prev_sq = (rand>0.5)*2-1;
    prev_data = (rand>0.5)*2-1;
    for r = 1:N
        if clk(r) == 1 && prev_clk == -1    % detect clock rising edge
            sq(r) = -prev_sq;               % logic square wave toggling (independent of M)
            data(r) = (rand>0.5)*2-1;        % logic random toggling (use M >= 1)
        else
            sq(r) = prev_sq;
            data(r) = prev_data;
        end
        prev_clk = clk(r);
        prev_sq = sq(r);
        prev_data = data(r);
    end

    if strcmp(mode, 'square')
        Y = abs((fft(sq, N)));
    elseif strcmp(mode, 'random')
        Y = abs((fft(data, N)));
    else
        Y = abs((fft(sq + data, N)));
    end
    YY = Y .* Y;
    YY = YY(1:N/2);
    YYsum = YYsum + YY;
end
YYsum = YYsum / M;

%% Plots
xfig = 300;
yfig = 200;
xfigw = 1000;
yfigw = 800;
dfig = 20;


%% Plot clock
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

plot(t, 1.2*clk, 'r', t, data, 'b')
grid on;
%ylim([-1.5, 1.5])

%% Plot spectrum
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

plot(f, 10*log10(YYsum))
grid on;
xlim([0, fs/2])
