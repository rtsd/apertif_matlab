%-----------------------------------------------------------------------------
%
% Copyright (C) 2019
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2019
%
% Purpose : Model wrapping in the beamformer
%
% The internal beamformer sum can have significant bit growth. With 14 bit
% input at the ADC, log2(sqrt(1024)) = 5 bit extra for the PFB processing
% gain and log2(96) = 6.6 bit extra for the BF gain, the maximum internal
% beamformer sum is about 25 bit. 
% For the final beamformer sum only w = 8 bits will be output. Therefor
% it seems a waste to still have to internally use 25 bits that require
% logic resources and IO resources on the ring. If the wrapped sum is used
% then less bits need to be used. Some extra bits are still needed to be
% able to distinguish wrapped final sums from correct final sums. Instead
% of 25 bits, e.g. use and transport 16 bit beamlet sums within the
% beamformer.
%
% The suppression of the beamformer outside the main beam makes that RFI
% outside the main beam may still fit in the w = 8 bit final sum. 
% However intermediate beamformer sums can exceed this w = 8 bit output
% range. If the intermediate sum is clipped, then the output will be 
% corrupted, because the clipping also flattens the weak astronomical
% signal that is on top of the RFI. If the intermediate sums are wrapped,
% then the information of the astronomical signal is preserved. In the
% final sum the RFI is then still attenuated and the astronomical signal
% remains undisturbed.
%
% The astronomical signal is weak in the sense that without RFI it fits
% in the w = 8 bit intermediate sum and final beamlet sum. The RFI signal
% is strong in the sense that it causes intermediate beamlet sums to 
% overflow, but it is weak enough to still fits in the w = 8 bit beamlet
% output sum, provided that the RFI is not in the direction of the main
% beam.

clear all;
close all;
fig=0;

%% Settings
c = 3e8;        % speed of light
N_theta= 1000;  % Number of beam angles to evaluate over 90 degrees
N_ant = 96;     % Number of antennas in array
f = 50e6;       % RF frequency in LBA band
L = c/f;        % RF wave length
b = 0.8*L;      % distance between antennas, choose < L to have one main lobe
w = 8;          % number of bits in beamlet
A = 3;          % amplitude of weak astronomical signal, choose A << R
R = 30;         % amplitude of strong RFI signal, choose R < 2**(w-1)

disp(sprintf(['. RF frequency        = %5.1f MHz\n', ...
              '. RF wave length      = %5.1f m\n', ...
              '. Antenna distance    = %5.1f m\n', ...
              '. Number of antennas  = %d\n'], ...
                  f/1e6, ...
                  L, ...
                  b, ...
                  N_ant));

%% Random logic signal toggling at clock frequency fclk
s_period = 2^w;
s_mod    = 2^(w-1);
s_max    =  s_mod-1;
s_min    = -s_mod;

thetas = 90 * (0:N_theta-1)/N_theta;
DTs = b / c * sin(thetas*2*pi/360);
phis = 2*pi*f*DTs;
ants = 0:N_ant-1;
bf_sums_maxs = zeros(1, N_theta);
bf_sums = zeros(1, N_theta);
bf_sum_wraps = zeros(1, N_theta);
bf_sum_clips = zeros(1, N_theta);
d_wraps = zeros(1, N_theta);
d_clips = zeros(1, N_theta);
for I = 1:N_theta
    % Calculate beamformer sum, using intermediate wrapping or clipping
    phi = phis(I);
    kphis = phi*ants;
    % Sort kphis to have worst case order where the BF inputs first add
    % all up constructively and then all down constructively, to have
    % the largest intermediate BF sum results.
    kphis = sort(mod(phi*ants, 2*pi));
    s = 0;
    s_wrap = 0;
    s_clip = 0;
    for k = 1:N_ant
        a = round(A + R*cos(kphis(k)));
        %a = round(A + R*sin(kphis(k)));
        s = s + a;
        if abs(s) > bf_sums_maxs(I)
            bf_sums_maxs(I) = abs(s);
        end
        s_wrap = wrap(s_wrap + a, s_period);
        s_clip = clip(s_clip + a, s_min, s_max);
    end
    bf_sums(I)      = s;
    bf_sum_wraps(I) = s_wrap;
    bf_sum_clips(I) = s_clip;
    % Derive difference with full range final beamformwer sum
    d_final_wraps(I) = s - s_wrap;
    d_final_clips(I) = s - s_clip;
    % Derive difference with w bit output beamformwer sum
    d_output_wraps(I) = wrap(s, s_period) - s_wrap;
    d_output_clips(I) = clip(s, s_min, s_max) - s_clip;
end

%% Plots
xfig = 300;
yfig = 200;
xfigw = 1000;
yfigw = 800;
dfig = 20;


%% Plot final beamformer sums
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

plot(thetas, bf_sums, 'k', thetas, bf_sum_wraps, 'r--', thetas, bf_sum_clips, 'b--');
title('Final beamformer sums');
xlabel('Beam angle [degrees]');
ylabel('Final sum');
grid on;


%% Plot abs maximum of intermediate beamformer sums
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

plot(thetas, bf_sums_maxs, 'r');
title('Maximum of intermediate beamformer sums');
xlabel('Beam angle [degrees]');
ylabel('Intermediate maximum sum');
grid on;


%% Plot differences between final sum and wrapped sum, clipped sum
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

plot(thetas, d_final_wraps, 'r', thetas, d_final_clips, 'b--');
title('Difference final beamformer sums');
xlabel('Beam angle [degrees]');
ylabel('Difference of final sum');
grid on;


%% Plot differences between output sum and wrapped sum, clipped sum
fig=fig+1;
figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
figure(fig);

plot(thetas, d_output_wraps, 'r', thetas, d_output_clips, 'b--');
title('Difference output beamformer sums');
xlabel('Beam angle [degrees]');
ylabel('Difference of final sum');
grid on;

