%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : Select data from block of data
% Description :
%   Reorder serially, e.g. in time. The output ctrl.block_size =
%   length(ctrl.select), so equal to the number of selections from in_data.
%   The selection can be in any order and can also contain duplicates.
%   Therefore length(out_data) can be <, == or > than length(in_data).
%   The ctrl.select usesindexing from 0 (instead of from 1), so the
%   ctrl.select values depend must be with range 0:length(in_data)-1.
function [state, out_data] = reorder_serial(ctrl, in_data)

% Select output data
out_data = in_data(ctrl.select+1);
    
% Keep state for next call
state = ctrl;
