%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : Try uencode to determine optimum quantisation
% Description :
% Remark : It seems more appropriate to use round() instead of uencode(),
%          because it is more transparant and does not have the limitation
%          of Wbits <= 32 (although it has the limitation of the mantissa
%          a double).

clear all;
close all;
fig=0;

% User settings
Wbits = 4;

% - Use integer >= 1 to have value 0 in range of s for sq
% - Use even integer to have value 0.5 in range of s for sr
s_nof_value_per_bit = 4;

% Quantisation
s_max =  2^(Wbits-1)-1;
s_min = -2^(Wbits-1);
s_bit = 1;
s_res = 1/s_nof_value_per_bit;
s = s_min : s_res : s_max

% - Matlab uencode
su = double(uencode(s+s_bit/2, Wbits, s_max+s_bit/2, 'signed'));

% - Round half away from zero, so +0.5 --> 1, -0.5 --> -1.
sr = round(s);

% - Round half to +infinity, so +0.5 --> 1, -0.5 --> 0.
sf = floor(s+s_bit/2);

% Plot
fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(s, s, 'k', s, su, 'ro');
title(['Quantisation with uencode(s+0.5) ', num2str(Wbits), ' bits'] );
grid on;

fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(s, s, 'k', s, sr, 'ro');
title(['Quantisation with round(s) ', num2str(Wbits), ' bits'] );
grid on;

fig=fig+1;
figure('position', [300+fig*20 200-fig*20 1000 800]);
figure(fig);
plot(s, s, 'k', s, sf, 'ro');
title(['Quantisation with floor(s+0.5) ', num2str(Wbits), ' bits'] );
grid on;
