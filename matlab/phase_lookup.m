%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: Eric Kooistra, 2016
%
% Purpose : Calculate look up table for (re, im) from phi
%

clear all;
close all;

N = 1024;
phi = (0:N-1) * 2*pi / N;
degree = phi * 360/2/pi;

w = 9;                       % nof bits including sign bit
m = w - 1;                   % magnitude width

q_full_scale = 2^m;          % full scale = magnitude = amplitude
q_max =  q_full_scale - 1;   % maximum value
q_min = -q_full_scale;       % minimum value

% Map +-1 to +-q_max to have symmetrical quantized range
% Using quantize() is equivalent to direct scaling and rounding as can
% e.g. be verified by uncommenting the lines with quantize() and using
% diff and the saved files.

re = round(cos(phi) * q_max);
im = round(sin(phi) * q_max);
%re = round(quantize(cos(phi) * q_max, q_full_scale, w));
%im = round(quantize(sin(phi) * q_max, q_full_scale, w));

% Print
disp(sprintf (['Look up table for (re, im) from phi:\n', ...
               '. Number of phi per 2*pi          = %d\n', ...
               '. Number of bit for re and im     = %d\n', ...
               '. Maximum quantized re            = %5d\n', ...
               '. Maximum quantized im            = %5d\n', ...
               '. Minimum quantized re            = %5d\n', ...
               '. Minimum quantized im            = %5d\n'], ...
               N, w, max(re), max(im), min(re), min(im)));

% Plot
plot(degree, re, 'k', degree, im, 'r');
xlim([0 360]);
grid on;

% Save
file_name = sprintf('data/phase_lookup_%d.dat', N);
fid = fopen(file_name, 'w');
for p = 1:N
    fprintf(fid, '%8d%8d\n', re(p), im(p));
end
fclose(fid);

