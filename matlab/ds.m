%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : Doppler frequency shift (DS) per block of data
% Description :
%   Apply a frequency shift to the input data using a complex phasor.
%   All input data is for the same time ctrl.t so the same frequency
%   shift is applied to the whole block.

function [state, out_data] = ds(ctrl, in_data)

% Data processing
out_data = exp(j*2*pi*ctrl.shift_freq*ctrl.t) * in_data;

% Keep state next call
state = ctrl;
state.t = ctrl.t + ctrl.block_period;
