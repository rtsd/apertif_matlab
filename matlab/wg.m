%-----------------------------------------------------------------------------
%
% Copyright (C) 2016
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: E. Kooistra, 2016
%
% Purpose : Waveform generator per block of data
% Parameters:
%   ctrl.block_nr      Data block counter
%   ctrl.block_size    Data block size
%   ctrl.offset        DC offset
%   ctrl.agwn_sigma    Standard deviation of Additive White Gaussion Noise
%   ctrl.signal        'sinusoid', 'pulse'
%   ctrl.data_w        =0 for floating point, >0 for quantization
%   ctrl.overflow      'clip', 'wrap'
%
%   'sinusoid'
%   ctrl.ampl          Sinusoid amplitude
%   ctrl.freq          Sinusoid frequency
%   ctrl.df            Sinusoid chirp frequency
%   ctrl.phase         Sinusoid phase
%   ctrl.sop           Start of PSK shift index
%
%   'pulse'
%   ctrl.ampl            Pulse peak level
%   ctrl.sop             Start of pulse index
%   ctrl.period          Pulse period
%   ctrl.width           Pulse width
%   ctrl.width_remaining Pulse width remaining
%
% Description :
%   The WG data is normalized to full_scale = 1 so range [-1:1]. Data
%   outside this range is clipped after the DC offset defined by
%   ctrl.offset and the AGWN defined by ctrl.agwn_sigma have been added.
%
%   The WG returns a ctrl.name that can be:
%   . 'dc'      when ctrl.ampl=0 using ctrl.offset
%   . 'noise'   when ctrl.ampl=0 and ctrl.offset=0 and ctrl.agwn_sigma~=0
%   when ctrl.signal='sinusoid':
%   . 'sinusoid'     when ctrl.ampl>0
%   . 'phasor'       when ctrl.ampl>0 and ctrl.complex
%   . 'psk'          when ctrl.ampl>0 and ctrl.psk>0
%   . '<name>_chirp' when ctrl.ampl>0 and ctrl.df>0
%   when ctrl.signal='pulse':
%   . 'pulse'        when ctrl.ampl>0
%   . 'impulse'      when ctrl.ampl>0 and ctrl.width=1
%   . 'square'       when ctrl.ampl>0 and ctrl.width=ctrl.period/2
%   . '<name>_chirp' when mod(ctrl.period, ctrl.block_size)~=0
%   when noise or AGWN then
%   . '<name>_agwn'    when ctrl.offset~=0 or ctrl.ampl~=0
%   . '<name>_complex' when ctrl.complex
%   
%   The WG output is real when ctrl.complex = false. When ctrl.complex =
%   true the sinusoid becomes a complex exponential and the noise becomes
%   complex noise.
%
% * ctrl.overflow
%   For floating point data (ctrl_data_w=0) and then ctrl.overflow='clip'
%   makes the analogue data clip to +-full_scale. Therefore use default
%   ctrl.overflow='clip' to model ADC data. For floating point
%   data ctrl.overflow ~= 'clip' has no effect on the  data.
%   For fixed point data (ctrl_data_w>0) the data is digitized to
%   ctrl_data_w number of bits. With ctrl.overflow = 'clip' the quantized
%   data gets clipped and with ctrl.overflow = 'wrap' the quantized data
%   will wrap.
%
% * Sinusoid:
%   Create a sinusoid waveform with ctrl.ampl, ctrl.freq and ctrl.phase.
%
%   The ctrl.freq = k*fs/ctrl.block_size, where k=0:ctrl.block_size/2
%   is the number of periods per block and fs=1 the normalized sample
%   frequency. The frequency of the output sinus is only exact when k is
%   an integer, otherwise the generated frequency will have some jitter
%   and only be exact on average. The ctrl.phase is the initial phase of
%   a block normalize to 2*pi.
%
%   For ctrl.data_w=0 the output is floating point and then there is no
%   clipping for ctrl.ampl <= 1 (and no AGWN and no DC offset).
%   For ctrl.data_w>0 the output is quantized and then for ctrl.ampl = 1
%   the most postive value just clips.
%
%   - Simple PSK modulation:
%   The sinusiod can be phase shift key (PSK) modulated by setting ctrl.psk
%   > 0. When ctrl.psk = 0 then no PSK of the sinusoid, else ctrl.psk sets
%   the number of PSK phases (2, 4, 8, ...). The simple PSK modelling 
%   scheme is used as very simple model for DAB (digital audio broadcast).
%   The PSK rate is set relative to the block rate by ctrl.psk_rate. The
%   phase shift can be applied in a step, but more realisticly it is
%   applied smoothly using ctrl.psk_smooth = 1 to keep the PSK signal
%   narrow band. The ctrl.dpsk_phase needs to be initialized to 0 and is
%   only used to keep the state between blocks.
%
%   - Chirp:
%   With ctrl.df ~= 0 the WG creates a chirp by increasing the freq by
%   ctrl.df after every block. For sufficiently small ctrl.df it is fine
%   to keep freq fixed during each block.
%
% * Pulser:
%   Create a pulse with peak level ctrl.ampl that starts at index
%   ctrl.sop when this falls within the block. The pulse is active for
%   ctrl.width and the pulse period is set by ctrl.period. The ctrl.width
%   must be < ctrl.period.Note that by using:
%   . ctrl.width = 1 the pulse becomes an 'impulse'
%   . ctrl.width = ctrl.period/2 the pulse becomes a 'square' wave
%   . ctrl.offset = -ctrl.ampl/2 the pulse DC level can be controlled
%   . mod(ctrl.period, ctrl.block_size) = 0 there is 1 or 0 pulse per block
%   . ctrl.period = ctrl.block_size+1 the 1 pulse moves (chirps) through
%                   the blocks
%   . ctrl.period < ctrl.block_size there are multiple pulses per block
%
% * AGWN:
%   With ctrl.agwn_sigma>0 white Gaussian noise with standard deviation
%   ctrl.agwn_sigma is added to the signal. New noise values are calculated
%   and added for every block of data. AGWN can be used to apply a sinusiod
%   with ctrl.ampl < quantisation step (1 lsb) that is burried in the
%   noise with ctrl.agwn_sigma ~= few quantisation steps. For 4 bit data
%   using ctrl.agwn_sigma between 1.25 lsb and 2.5 lsb provides a good
%   compromise between limiting the increase in system noise due to
%   quantization and limiting the number of clipped samples. For an ADC
%   with more than 4 bits the extra bits are used to avoid clipping due
%   to RFI.

function [state, data] = wg(ctrl)
% Data processing
data = zeros(1, ctrl.block_size);  % initialize to zero
if ctrl.ampl>0
    % Calculate functional waveform
    if strcmp(ctrl.signal, 'sinusoid')
        if ctrl.complex
            ctrl.name = 'phasor';
        else
            ctrl.name = 'sinusoid';
        end
        if ctrl.df>0
            ctrl.name = [ctrl.name, '_chirp'];
        end
        
        t = 0:ctrl.block_size-1;
        %freq = mod(ctrl.freq, 0.5);  % sweep 0 up to fs/2 wrap 0, 0 to fs/2, etc  
        freq = ctrl.freq;            % sweep 0 up to inf, so effectively sweep 0 up to fs/2, downto 0 up to fs/2, etc
        
        % Simple model for PSK modulation of the sinusoid
        if ctrl.psk
            ctrl.name = 'psk';
            psk_ref = sum(1:ctrl.psk)/ctrl.psk;
            for bi = 0:ctrl.block_size-1
                bI = bi+1;
                if bi == ctrl.sop
                    % Set index for next phase shift sop
                    ctrl.sop = ctrl.sop + floor(ctrl.period);
                    % Determine and apply normalized PSK phase shift
                    psk_phase = (randi(ctrl.psk,1)-psk_ref)/ctrl.psk;
                    if ctrl.psk_smooth
                        ctrl.dpsk_phase = psk_phase/ctrl.period;       % apply phase shift smoothly
                    else
                        ctrl.dpsk_phase = 0;
                        ctrl.phase = ctrl.phase + psk_phase;           % apply phase shift in a step
                    end                        
                end
                ctrl.phase = ctrl.phase + ctrl.dpsk_phase;
                phases(bI) = ctrl.phase;
            end
            % Update index for next phase shift sop
            if ctrl.sop >= ctrl.block_size
                ctrl.sop = ctrl.sop - ctrl.block_size;
            end
        else
            phases = ctrl.phase;
        end
        
        if ctrl.complex
            data = ctrl.ampl * exp(i*2*pi*(freq*t+phases));
        else
            data = ctrl.ampl * sin(2*pi*(freq*t+phases));
        end
    elseif strcmp(ctrl.signal, 'pulse')
        ctrl.name = 'pulse';
        if ctrl.width == 1
            ctrl.name = 'impulse';
        elseif ctrl.width == ctrl.period/2
            ctrl.name = 'square';
        end
        if mod(ctrl.period, ctrl.block_size)~=0
            ctrl.name = [ctrl.name, '_chirp'];
        end 
        % Handle a pulse that extends over the block boundary means of ctrl.width_remaining
        if ctrl.width_remaining > 0
            if ctrl.width_remaining<ctrl.block_size
                data(1:ctrl.width_remaining) = ctrl.ampl;         % this data block ends a pulse
                ctrl.width_remaining = 0;
            else
                data = data + ctrl.ampl;                          % this data block continues a pulse
                ctrl.width_remaining = ctrl.width_remaining - ctrl.block_size;
            end
        end
        % Handle start of a new pulse by means of ctrl.sop
        t_begin = floor(ctrl.sop);        % use floor() to allow fractional ctrl.period
        while t_begin < ctrl.block_size
            t_end = t_begin+ctrl.width-1;
            if t_end < ctrl.block_size
                data(1 + (t_begin:t_end)) = ctrl.ampl;              % this data block starts and ends a pulse
                ctrl.width_remaining = 0;
            else
                data(1 + (t_begin:ctrl.block_size-1)) = ctrl.ampl;  % this data block starts a pulse
                ctrl.width_remaining = ctrl.width - (ctrl.block_size-1-t_begin);
            end
            ctrl.sop = ctrl.sop + ctrl.period;
            t_begin = floor(ctrl.sop);
        end
    else
        ctrl.name = 'dc';
    end
else
    ctrl.name = 'dc';
end

% DC offset
data = data + ctrl.offset;

% Additive Gaussian White Noise
if ctrl.agwn_sigma>0
    if ctrl.ampl==0 && ctrl.offset==0
        ctrl.name = 'noise';
    else 
        ctrl.name = [ctrl.name, '_agwn'];
    end
    if ctrl.complex
        data = data + ctrl.agwn_sigma * (randn(1, ctrl.block_size) + i*randn(1, ctrl.block_size));
        ctrl.name = [ctrl.name, '_complex'];
    else
        data = data + ctrl.agwn_sigma * randn(1, ctrl.block_size);
    end
end

% Analogue full scale clipping at +-1 by an ADC
full_scale = 1;
if strcmp(ctrl.overflow, 'clip')
    data(data> full_scale) =  full_scale;
    data(data<-full_scale) = -full_scale;
end

% Normalize output to [-1 1]
% Not necessary, because by default the WG has full_scale = 1.

% Quantization
if ctrl.data_w>0
    data = quantize(data, full_scale, ctrl.data_w, 'half_away', ctrl.overflow);
end

% Keep state for next call
state = ctrl;
state.block_nr = ctrl.block_nr+1;
if strcmp(ctrl.signal, 'sinusoid')
    state.freq = ctrl.freq + ctrl.df;
    state.phase = ctrl.phase + ctrl.freq*ctrl.block_size;
elseif strcmp(ctrl.signal, 'pulse')
    state.sop = ctrl.sop - ctrl.block_size;                % update start of pending next pulse
end

