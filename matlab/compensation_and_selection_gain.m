%-----------------------------------------------------------------------------
%
% Copyright (C) 2017
% ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
% P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%-----------------------------------------------------------------------------
% Author: Eric Kooistra, 30 mar 2017
%
% Purpose : Model requantization compensation gain and selection gain
%   
% Description :
%   When a gain is applied to an input value then the output value has
%   bit growth: out_dat_w = in_dat_w + gain_w. In addition the output
%   value may also be requantized to req_dat_w. The script has two aims:
%
%   1 Compensate for some input loss by scaling the output back to the 
%     nominal output level using a gain = 1/loss. The loss may occur e.g.
%     in case one or more inputs in a beamformer are switched off. If
%     nominal the beamformer has 8 inputs, but now it has 7, then the 
%     gain = 8/7 to compensate the loss of 7/8 and thus preserve the
%     nominal level of 8/8 = 1.
%
%   2 Determine how much gain_w is needed to support selecting the
%     appropriate range of out_dat bits for fixed requantisation from
%     in_dat_w via out_dat_w to to req_dat_w.
%
%   Using compensation gain = 1/loss (1) and selection gain (2) the
%   in_dat is scaled such that the fixed requantization can be set for
%   the nominal input level (1) and has the optimum balance between
%   dynamic range and resolution (2).
%   

clear all;
close all;
plot_enable = false;

%% (1) Compensation gain to restore nominal input level
%   The gain can be also used to scale the input data to compensate for
%   some input loss = 7/8, e.g. gain = 1/loss = 8/7 = 1.143. In this case
%   the number of bits for the requantized data remains the same as for
%   the input data, the gain is used to restore the nominal input level.
%   If this compensation gain (1) is combined with the selection gain (2),
%   then it is sufficient to consider only compensation gain < 2.
%  
%   Suppose:
%     in_dat_w  =  4
%     req_dat_w =  4 = in_dat_w (consider only scaling here, no change in 
%                                number of bits.)
%   Choose:
%     gain_w    =  5
%     out_dat_w =  8 = gain_w + in_dat_w - 1  (-1 to skip doub le sign bit)
%   The fixed requantization setting for out_dat selects range:
%     lsb_w     =  3 = gain_w - 2
%     msb_w     =  1 =  out_dat_w               -(req_dat_w+lsb_w)
%                    = (gain_w +  in_dat_w - 1) - req_dat_w-lsb_w
%                    =  gain_w + req_dat_w - 1  - req_dat_w-(gain_w-2)
%                    = 1 (fixed)
%   so:
%     req_dat = out_dat[req_dat_w+lsb_w-1:lsb_w]
%             = out_dat[6:3]
%   Hence the out_dat already skips the double sign bit, then from out_dat
%   msb_w = 1 MSbit get truncated and lsb_w = gain_w - 2 get rounded, as
%   shown by:
%  
%                               [3  2  1  0]
%     in_dat          =          s  i  j  k
%  
%                               [4  3  2  1  0]
%     gain    2**( 0) = 1        s  1  0  0  0
%     gain    2**(-1) = 0.5      s  0  1  0  0
%     gain    2**(-2) = 0.25     s  0  0  1  0
%     gain    2**(-3) = 0.125    s  0  0  0  1 
%       
%                               [7  6  5  4  3  2  1  0]
%     out_dat 2**( 0) = 1        s  s  i  j  k  0  0  0  0  0  0
%     out_dat 2**(-1) = 0.5      s  s  s  i  j  k  0  0  0  0  0
%     out_dat 2**(-2) = 0.25     s  s  s  s  i  j  k  0  0  0  0
%     out_dat 2**(-3) = 0.125    s  s  s  s  s  i  j  k  0  0  0
%     out_dat 2**(-4) = 0.0625   s  s  s  s  s  s  i  j  k  0  0
%     out_dat 2**(-5) = 0.03125  s  s  s  s  s  s  s  i  j  k  0
%  
%     req_dat         =             s  i  j  k
%   
%   A compensation gain of 1 yields req_dat = in_dat. The compensation
%   gain can be set between 0.5 and 2. The gain_w should be >= req_dat_w.
%   From the simulation it follows that in fact the gain_w should be
%   about 3 bits more to avoid too much requantization noise, so choose
%   compensation gain_w = req_dat_w + 3.
%
%   The compensation gain of 1 corresponds to [s 1 ...], independent of
%   gain_w, and gain_w >= 2 (because lsb_w >= 0).

in_dat_w = 8;

N = 1000;
t = 0:N-1;

in_full_scale = 1;
loss = 0.99;
loss = 7/8;
%loss = 1;
if loss<=1
    ampl = loss;
else
    ampl = 1/loss;  % avoid input overflow
end
in_dat = ampl * rand(1, N);
q_in_dat = quantize(in_dat, in_full_scale, in_dat_w);
q_in_dat_snr = 10*log10(sum(abs(in_dat).^2) / sum(abs(in_dat - q_in_dat).^2));

disp(sprintf('. ampl               = %f', ampl));
disp(sprintf('. in_dat_w           = %d', in_dat_w));
disp(sprintf('. q_in_dat SNR       = %6.3f (theoretical %6.3f)', q_in_dat_snr, in_dat_w * db(2)));

in_dat_loss = in_dat * loss;
q_in_dat_loss = quantize(in_dat_loss, in_full_scale, in_dat_w);

q_in_dat_loss_snr = 10*log10(sum(abs(in_dat_loss).^2) / sum(abs(in_dat_loss - q_in_dat_loss).^2));

gain = 1/loss;
gain_full_scale = 2;

disp(sprintf('. loss               = %f', loss));
disp(sprintf('. gain = 1/loss      = %f', gain));
disp(sprintf('. q_in_dat_loss SNR  = %f', q_in_dat_loss_snr));

for gain_w = floor(in_dat_w/2):2*in_dat_w
    q_gain = quantize(gain, gain_full_scale, gain_w);

    q_in_dat_recovered = q_in_dat_loss * q_gain;
    q_in_dat_recovered_snr = 10*log10(sum(abs(in_dat).^2) / sum(abs(in_dat - q_in_dat_recovered).^2));

    disp(sprintf('. q_in_dat_recovered SNR    = %6.3f  (gain_w = %2d, q_gain = %f)', q_in_dat_recovered_snr, gain_w, q_gain));
end


%% (2) Selection gain to fit fixed requantization to less bits
%   The input data needs to be requantized into less bits. By means of a
%   gain it is possible to scale the input such that either the MSbit or
%   the LSbits of in_dat are preserved, or somewhere in between. More
%   MSbits means more dynamic range, more LSbits mean more resolution.
%
%   The output data width grows due to the product of gain and input
%   data. Choose out_dat_w = gain_w + in_dat_w - 1. This skips the
%   double sign bit of the product and assumes that full scale amplitude
%   of the complex gain = full scale real = full scale imag to not cause
%   overflow. The output data is requantization to req_dat_w.
%  
%   Suppose:
%     in_dat_w  = 6   nof bits in the input data
%     req_dat_w = 4   nof bits after requantization
%   then choose:
%     reduce_w  =  2 = in_dat_w - req_dat_w
%     gain_w    =  4 = reduce_w + 2, +1 for sign bit and +1 to fit +2**reduce_w
%     out_dat_w =  9 = gain_w + in_dat_w - 1
%   such that:
%     gain = 2**0 =  1 :   keep the MSbits and  round 2 LSbits from in_dat
%     gain = 2**1 =  2 : truncate 1 MSbits and  round 1 LSbits from in_dat
%     gain = 2**2 =  4 : truncate 2 MSbits and keep the LSbits from in_dat
%   the fixed requantization setting for req_dat selects range:
%     lsb_w     =  2 = reduce_w
%     msb_w     =  3 =  out_dat_w              - (req_dat_w+reduce_w)
%                    = (gain_w + in_dat_w - 1) -  req_dat_w-reduce_w
%                    =  gain_w + in_dat_w - 1  -  req_dat_w-(in_dat_w-req_dat_w)
%                    =  gain_w + in_dat_w - 1  -  req_dat_w- in_dat_w+req_dat_w
%                    =  gain_w-1
%   so:
%     req_dat = out_dat[req_dat_w+reduce_w-1:reduce_w] = out_dat[5:2]
%   so gain_w-1 = reduce_w + 1 = 3 MSbits get truncated and gain_w-2 =
%   reduce_w = 2 LSbits get rounded, as shown by:
%   
%                            [5  4  3  2  1  0]
%     in_dat             =    s  a  b  c  d  e
%   
%                            [3  2  1  0]
%     gain    2**2 =   4 =    s  1  0  0
%     gain    2**1 =   2 =    s  0  1  0
%     gain    2**0 =   1 =    s  0  0  1
%   
%                            [8  7  6  5  4  3  2  1  0]
%     out_dat 2**2 =   4 =    s  s  a  b  c  d  e  0  0
%     out_dat 2**1 =   2 =    s  s  s  a  b  c  d  e  0
%     out_dat 2**0 =   1 =    s  s  s  s  a  b  c  d  e
%  
%                                    [ 3  2  1  0]
%     req_dat            =             s  c  d  e         gain = 2**2 = 4
%     req_dat            =             s  b  c  d         gain = 2**2 = 2
%     req_dat            =             s  a  b  c         gain = 2**0 = 1
%
%   The selection gain is 2^0 = 1 to preserve the MSbits and 2^reduce_w
%   to preserve the LSbits. The selection gain of 1 corresponds to
%   [s ... 1], independent of gain_w, and gain_w >= 2 (because reduce_w
%   >= 0).

%%  Combining compensation gain (1) with selection gain (2)
%
%   The compensation gain and selection gain can be implemented as two
%   seperate stages, but the aim is to implement them together in one
%   stage. All selection gains maps to compensation gain = 1. The corner
%   case is selection gain = 1, because then the compensation gain cannot
%   reuse the LSBits of the selection gain. From (1) and (2) above it
%   followed that:
%
%   1) The compensation gain of 1 corresponds to [s 1 ...], independent
%      of gain_w, and gain_w >= 2 (because lsb_w >= 0).
%   2) The selection gain of 1 corresponds to [s ... 1], independent
%      of gain_w, and gain_w >= 2 (because reduce_w >= 0).
%
%   Therefore the gain_w for the combination becomes:
%
%     gain_w = sel_gain_w + comp_gain_w - 2
%
%   where -2 is due to that s 1 from the compensation gain can reuse the
%   sign bit and one LSBit of the selection gain.
%
%   For example:
%     in_dat_w  = 12  nof bits in the input data
%     req_dat_w =  8  nof bits after requantization
%   then choose:
%     reduce_w    =  4 = in_dat_w - req_dat_w
%     sel_gain_w  =  6 = reduce_w + 2, +1 for sign bit and +1 to fit +2**reduce_w
%     comp_gain_w = 11 = req_dat_w + 3, +3 based on this simulation
%
%   The assumption is that the compensation gain must also preserve the
%   resolution when the selection gain 1 to select the MSbits.
%
%     gain_w    = 15 = sel_gain_w + comp_gain_w - 2 = in_dat_w + 3
%     out_dat_w = 26 = gain_w + in_dat_w - 1 = 2*in_dat_w + 2
%
%   The fixed requantization setting for req_dat selects range:
%     lsb_w     = 13 = reduce_w + comp_gain_w - 2
%     msb_w     =  5 = sel_gain_w-1
%     req_dat = out_dat[req_dat_w+lsb_w-1:lsb_w]
%             = out_dat[20:13]
%
%   The out_dat range is [25:0], so lsb_w = 13 LSbits get rounded and
%   msb_w = 5 bits get truncated.
%   In an implementation these truncated bits could be ommitted from the
%   start.

if plot_enable
    %% Plot results
    fig=0;
    xfig = 300;
    yfig = 200;
    xfigw = 1000;
    yfigw = 800;
    dfig = 20;


    %% Plot data
    fig=fig+1;
    figure('position', [xfig+fig*dfig yfig-fig*dfig xfigw yfigw]);
    figure(fig);
    plot(t, q_in_dat_recovered)
    title(sprintf('Input data'));
    grid on;
end
