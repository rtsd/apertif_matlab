# Apertif MATLAB

The apertif_matlab package contains MATLAB scripts and Python scripts that model the subband filterbank and the channel filterbank that are used the Apertif radio telescope of [ASTRON] [1].

Both filterbanks are critically sampled, polyphase filterbanks (PFB). The subband filterbank is a wideband polyphase filterbank (WPFB), meaning that the input sample rate of 800 MSps is a wideband factor 4 higher than the processing rate of 200 MHz. Both filterbanks are implemented in VHDL and run on FPGAs of the UniBoard [2].

For more information on the the apertif_matlab package see:

* The matlab/apertif_matlab_readme.txt briefly decribes each script
* The matlab/ASTRON_RP_010888_Apertif_Arts_firmware_model.pdf describes the quantization model of the subband filterbank and channel filterbank. The Python script apertif_arts_firmware_model.py runs the model.

To run the python code it is necessary to first get the desp_tools from [3].

## References

[1] W.A. van Cappellen et al., "Apertif, Phased Array Feeds for the Westerbork Synthesis Radio Telescope", unpublished, 2021

[2] "The application of UniBoard as a beam former for APERTIF”, A. Gunst, A. Szomoru, G. Schoonderbeek, E. Kooistra, D. van der Schuur, H. Pepping, Experimental Astronomy, Volume 37, Issue 1, pp 55-67, February 2014, doi:1
0.1007/s10686-013-9366-x.

[3] https://github.com/erkooi/desp_tools


If you use (part of) the **apertif_matlab** package please attribute the use as indicated this citation NOTICE:

### [NOTICE](NOTICE.md)

The apertif_matlab package is Open Source and available under the following LICENSE:

### [LICENSE](LICENSE.md)


[ASTRON]:https://www.astron.nl
